const { Buffer, Program, Shader, Attribute, Type, Context, Uniform } = require( '../lib/webgl/gl' );
const { clamp, rand, round } = require( '../lib/math/math' );
const Timestep = require( "../lib/timestep" );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const fs = require( 'fs' );

function readFile( filepath )
{
	let r = fs.readFileSync( filepath, 'utf-8' );
	return r;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
let prog, verts, vertbuffer, context, uRes, uScale;

let Color  = new Type.Composite( { r: Type.ubyte, g: Type.ubyte, b: Type.ubyte, a: Type.ubyte } );
let Vertex = new Type.Composite( { point: Type.Vector3, color: Color } );
let time   = new Timestep(1,render);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
window.size = () => ( [ window.innerWidth, window.innerHeight ] );
window.addEventListener( "load", ( event ) =>
{
	context = new Context();
	context.makeCurrent();
	context.resize( ...window.size() );

	document.body.appendChild( context.canvas );

	uRes   = new Uniform.Vector2( 'Resolution', window.size() );
	uScale = new Uniform.Float( 'Scale', 1.0 );

	run();
} );
window.addEventListener( "resize", (event)=>
{
    context.resize( ...window.size() );
	uRes.set( ...window.size() );
} );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

let ri=()=>( (rand()*255)|0);
let r=(x)=>( (rand()*x  )|0);
let NUM = 10000;

function run()
{
	context.clearColor( 0,0,0,1 ).blend().clear();
	let [w,h] = window.size();
	verts  = Vertex.Array( NUM );
	verts.each( (v)=>(
		[v.point.x,v.point.y,v.point.z          ] = [r(w),r(h),0],
		[v.color.r,v.color.g,v.color.b,v.color.a] = [ri(),ri(),ri(),255]
		//,console.log( v.toString() )
		) );

	prog = new Program( new Shader( Shader.vertex,   readFile( "./.ants/shader/entity.vert.glslx" ) ),
		                new Shader( Shader.fragment, readFile( "./.ants/shader/entity.frag.glslx" ) ) );

	vertbuffer = new Buffer( gl.ARRAY_BUFFER, gl.DYNAMIC_DRAW ).bind().data( verts.data ).unbind();
	time.start();

}
function wrap( v, l, i=(v|0), d=(v-i) )
{
	return (v%l+d);
}
function update()
{
	let p, n, cx, cy, v, [w,h] = context.size;
	for( let i=0; i<NUM; i++ )
	{
		p = verts[i].point;
		let n = (rand()*5);
		cx = (round(rand()*2-1)*n);
		cy = (round(rand()*2-1)*n);

		[p.x,p.y] = [wrap(p.x+cx,w), wrap(p.y+cy,h) ];
	}
	vertbuffer.bind().data( verts.data ).unbind();
}
function render()
{
	update();

	context.clear();

    vertbuffer.bind();
    prog.enable();
	uRes.enable();
	uScale.enable();
	Vertex.enableGlAttributes();

    context.draw( gl.POINTS, 0, NUM );

	Vertex.disableGlAttributes();
    prog.disable();
    vertbuffer.unbind();
}
