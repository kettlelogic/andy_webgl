const { Buffer, Program, Shader, Attribute, Type, Context, Uniform } = require( '../lib/webgl/gl' );
const { clamp, rand, round, deg2rad, Vector3, RGBA } = require( '../lib/math/math' );
const Camera   = require( '../lib/3d/camera'   );
const App      = require( '../lib/application' );
const Mouse    = require( '../lib/io/mouse'    );
const Keyboard = require('../lib/io/keyboard'  );
const Timestep = require( "../lib/timestep"    );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const fs = require( 'fs' );

function readFile( filepath )
{
	let r = fs.readFileSync( filepath, 'utf-8' );
	return r;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
let umodel, cam, prog, indicies, verts, indxbuffer, vertbuffer, context, uRes, uScale;

let Vertex = new Type.Composite( { point: Vector3, color: RGBA } );
let time   = new Timestep( 32, render );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
App.on( "load", ( event ) =>
{
	context = new Context().makeCurrent().resize( ...App.size );
	document.body.appendChild( context.canvas );
	Mouse.attach();

	cam = new Camera( 75 );

	run();
} );
App.on( "resize", (event)=>(context.resize(...App.size ) ) );

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function run()
{
	context.clearColor( 0,0,0,1 ).blend().cull(gl.FRONT).clear();

	indicies = new Uint16Array([0,1,3,0,3,2,1,5,7,1,7,3,5,4,6,5,6,7,4,0,2,4,2,6,2,3,7,2,7,6,4,1,0,4,5,1]);
	verts    = Vertex.Array( 8 );
	verts[0].point.set(-0.5, -0.5,  0.5); verts[0].color.set(255,   1,   1, 255);
	verts[1].point.set( 0.5, -0.5,  0.5); verts[1].color.set(  1, 255,   1, 255);
	verts[2].point.set(-0.5,  0.5,  0.5); verts[2].color.set(  1,   1, 255, 255);
	verts[3].point.set( 0.5,  0.5,  0.5); verts[3].color.set(  1, 255, 255, 255);
	verts[4].point.set(-0.5, -0.5, -0.5); verts[4].color.set(255, 255, 255, 255);
	verts[5].point.set( 0.5, -0.5, -0.5); verts[5].color.set(255, 255,   1, 255);
	verts[6].point.set(-0.5,  0.5, -0.5); verts[6].color.set(255,   1, 255, 255);
	verts[7].point.set( 0.5,  0.5, -0.5); verts[7].color.set(255, 255, 255, 255);

	prog = new Program( new Shader( Shader.vertex,   readFile( "./.ants/shader/cam.vert.glslx" ) ),
		                new Shader( Shader.fragment, readFile( "./.ants/shader/cam.frag.glslx" ) ) );

	//cam.translateZ( 10 );
	console.log( cam );
	Mouse.delta.on( 'change', (m)=>(cam.update(m)) );
	Keyboard.bind( 'W', ()=>(cam.translateZ( -1 )) );
	Keyboard.bind( 'S', ()=>(cam.translateZ(  1 )) );
	Keyboard.bind( 'A', ()=>(cam.translateX(  1 )) );
	Keyboard.bind( 'D', ()=>(cam.translateX( -1 )) );
	Keyboard.bind( 'E', ()=>(cam.translateY(  1 )) );
	Keyboard.bind( 'Q', ()=>(cam.translateY( -1 )) );

	umodel = new Uniform.Matrix4('Model');

	vertbuffer = new Buffer( Buffer.verticies, Buffer.dynamic.darw ).bind().data( verts.data ).unbind();
	indxbuffer = new Buffer( Buffer.indicies,  Buffer.dynamic.darw ).bind().data( indicies   ).unbind();

	umodel.translate([0.0,0.0,-10.0]);
	umodel.rotateY( deg2rad(45) );
	//umodel.rotateX( deg2rad(45) );

	time.start();
}
function update()
{
	if( Mouse.isDown )
	{
		Mouse.delta.update();
	}
	if( cam.dirty ) cam.updateView();
	umodel.rotateY( deg2rad(5) );
}
function render()
{
	update();

	context.clear();

    vertbuffer.bind();
	indxbuffer.bind();
    prog.enable();
	cam.enable();
	umodel.enable();
	Vertex.enableGlAttributes();

    context.drawElements( gl.TRIANGLES, 36, gl.UNSIGNED_SHORT, 0 );

	Vertex.disableGlAttributes();
    prog.disable();
    vertbuffer.unbind();
	indxbuffer.unbind();
}