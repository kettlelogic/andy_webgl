
class Timestep
{
	constructor( fps, callback )
	{
		this.fps       = fps;
		this.duration  = 1000 / fps;
		this.delta     = 0.0;
		this.timer     = null;
		this.prev_time = 0.0;
		this.curr_time = 0.0;
		this.callback  = callback;
		this.running   = null;
	}
	start()
	{
		this.running = true;
		requestAnimationFrame( this.callback );
		this.timer   = setTimeout( ()=>(this.update()), this.duration );
	}
	update()
	{
		if( !this.running ) return;

		this.prev_time = this.curr_time;
		this.curr_time = performance.now();
		this.delta    += this.curr_time - this.prev_time;

		if( this.delta >= this.duration )
		{
			this.delta = 0.0;
			requestAnimationFrame( this.callback );
			clearTimeout( this.timer );
			this.timer   = setTimeout( ()=>(this.update()), this.duration );
		}
		else
		{
			clearTimeout( this.timer );
			this.timer = setTimeout( ()=>(this.update()), (this.duration-this.delta)|0 );
		}
	}
	stop()
	{
		clearTimeout( this.timer );
		this.running = false;
	}
}

module.exports = Timestep;