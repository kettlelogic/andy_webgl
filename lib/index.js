module.exports = {
    util:require("./util.js"),
    Timestep:require("./timestep.js"),
    event:require("./event"),
    clock:require("./clock.js"),
    Color:require("./math/color.js"),
    //error:require("./error.js"),
    //application:require("./application.js"),
    gl:require("./webgl/gl")
};
