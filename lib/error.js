
class MyError extends Error
{
  constructor(message)
  {
    super(message);
    this.message = message;
    this.name    = this.constructor;
  }
}

throw (new MyError('testError'));