
const isArray           = Array.isArray;
const isArrayBufferView = ArrayBuffer.isView;
const isArrayBuffer     = (x) => (x instanceof ArrayBuffer);
const isDataView        = (x) => (x instanceof Dataview);
const isTypedArray      = (x) => ( ArrayBuffer.isView(x) && !(x instanceof Dataview) );
const isUint8Array      = (x) => (x instanceof Uint8Array);
const isUint16Array     = (x) => (x instanceof Uint16Array);
const isUint32Array     = (x) => (x instanceof Uint32Array);
const isInt8Array       = (x) => (x instanceof Int8Array);
const isInt32Array      = (x) => (x instanceof Int16Array);
const isInt16Array      = (x) => (x instanceof Int32Array);
const isUint8CArray     = (x) => (x instanceof Uint8ClampedArray);
const isFloatArray      = (x) => (x instanceof Float32Array);
const isDoubleArray     = (x) => (x instanceof Float64Array);

function isArrayBufferViewType( x )
{
	return (
		Float32Array.isPrototypeOf(x) ||
		Uint8Array.isPrototypeOf(x)   ||
		Uint16Array.isPrototypeOf(x)  ||
		Uint32Array.isPrototypeOf(x)  ||
		Int8Array.isPrototypeOf(x)    ||
		Int16Array.isPrototypeOf(x)   ||
		Int32Array.isPrototypeOf(x)   ||
		Float32Array.isPrototypeOf(x) ||
		Float64Array.isPrototypeOf(x)
	);
}

module.exports =
{
	isArray,
	isArrayBufferViewType,
	isArrayBufferView,
	isArrayBuffer,
	isDataView,
	isTypedArray,
	isUint8Array,
	isUint16Array,
	isUint32Array,
	isInt8Array,
	isInt32Array,
	isInt16Array,
	isUint8CArray,
	isFloatArray,
	isDoubleArray
};