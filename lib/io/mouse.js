const Electron    = require( 'electron' );
const Screen      = require( 'electron' ).screen;
const Application = require( '../application' );
const Event       = require( '../event' );
const {Vector2}   = require( '../math/vector2' );

let   _position  = new Vector2();
let   _events    = new Event.Emitter();
let   _down      = false;

class MouseDelta extends Event.Emitter
{
	constructor()
	{
		super();
		this._value = new Vector2([0,0]);
		this._prev  = new Vector2([0,0]);
	}
	get xy()
	{
		return this._value;
	}
	get x()
	{
		return this._value[0];
	}
	get y()
	{
		return this._value[1];
	}
	reset()
	{
		this._prev.copy( Mouse.position );
	}
	update()
	{
		if( !Mouse.position.equals(this._prev) )
		{
			Vector2.subtract( this._prev, Mouse.position, this._value );
			this._prev.copy( Mouse.position );
			this.emit( 'change', this );
		}
	}
}

const _delta = new MouseDelta();

class Mouse
{
	static attach( el=document.body )
	{
		el.addEventListener( 'click',     (e)=>( Mouse.onClick(e) ) );
		el.addEventListener( 'mousedown', (e)=>( Mouse.onDown(e)  ) );
		el.addEventListener( 'mouseup',   (e)=>( Mouse.onUp(e)    ) );
	}
	static get delta()
	{
		return _delta;
	}
	static get position()
	{
		Vector2.sub( _position.setFromObject( Screen.getCursorScreenPoint() ), Application.position, _position );
		let x = ( _position.x >=0 && _position.y>=0 && _position.x <= Application.width && _position.y <= Application.height);
		return x ? _position : null;
	}
	static get isDown()
	{
		return _down;
	}
	// EVENT EMITTER //////////////////////////////////////////////////////////////////////////////
	static on( event, fn )
	{
		_events.on( event, fn );
		return Application;
	}
	static clear( event, fn )
	{
		_events.clear( event, fn );
		return Application;
	}
	static has( event, fn )
	{
		_events.has( event, fn );
		return Application;
	}
	static emit( event, ...args )
	{
		_events.emit( event, ...args );
		return Application;
	}
	// MOUSE EVENT CALLBACKS //////////////////////////////////////////////////////////////////////
	static onClick( e )
	{
		_events.emit( "click"  );
	}
	static onDown( e )
	{
		_events.emit( "down"  );
		_down = true;
		_delta.reset();
		console.log( 'down' );
	}
	static onUp( e )
	{
		_events.emit( "up"  );
		_down = false;
		_delta.reset();
		console.log( 'up' );
	}
}

module.exports = Mouse;