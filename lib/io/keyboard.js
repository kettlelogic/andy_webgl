
const Event = require( '../event' );

const _bindings = {};

class Keyboard
{
	static bind( code, func )
	{
		if( code.length === 1 )
		{
			if( code.match(/[a-z]/i) ) code = `Key${code}`;
			else                       code = `Digit${code}`;
		}
		if( !!_bindings[code] )
		{
			throw `key binding for ${code} already exists.`
		}
		_bindings[code] = func;
	}

	static onKeyDown( {code} )
	{
		if( !!_bindings[code] )
		{
			_bindings[code]();
		}
	}
	static onKeyUp( {code} )
	{

	}
}

if( window && window.addEventListener )
{
	window.addEventListener( 'keydown', Keyboard.onKeyDown );
	window.addEventListener( 'keyup',   Keyboard.onKeyUp   );
}

module.exports = Keyboard;