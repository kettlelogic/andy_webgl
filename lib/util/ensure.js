const {
	isArray,
	isBuffer,
	isDataView,
	isUint8Array,
	isUint16Array,
	isUint32Array,
	isInt8Array,
	isInt32Array,
	isInt16Array,
	isUint8ClampedArray,
	isFloat32Array,
	isFloat64Array,
	isTypedArray,
	isBufferView,
	isBufferInstance,
	isDataViewInstance,
	isUint8ArrayInstance,
	isUint16ArrayInstance,
	isUint32ArrayInstance,
	isInt8ArrayInstance,
	isInt32ArraYInstance,
	isInt16ArrayInstance,
	isUint8ClampedArrayInstance,
	isFloat32ArrayInstance,
	isFloat64ArrayInstance,
	isTypedArrayInstance,
	isBufferViewInstance
} = require( './check' );

function EnsureBufferViewSubType( subject )
{
	if( !isBufferView(subject) ) throw 'Invalid parameter: Expected an ArrayBufferView type';
	return subject;
}
function EnsureBufferViewType( subject )
{
	if( !isBufferView(subject) ) throw 'Invalid parameter: Expected an ArrayBufferView type';
	return subject;
}
function EnsureTypedArrayType(subject)
{
	if( !isTypedArray(subject) ) throw 'Invalid parameter: Expected an TypedArray type';
	return subject;
}

module.exports =
{
	EnsureBufferViewSubType,
	EnsureBufferViewType,
	EnsureTypedArrayType
};