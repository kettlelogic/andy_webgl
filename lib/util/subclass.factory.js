const { EnsureBufferViewType } = require( './ensure' );
const { isTypedArray         } = require( './check'  );

function Lookup( CACHE, TYPE, FUNC, ARGS, RESULT  )
{
	let key = TYPE.name + ARGS.toString();
	if( !(RESULT = CACHE.get( key )) )
	{
		return CACHE.set( key, (RESULT = FUNC( TYPE, ...ARGS )) ), RESULT;
	}
	return RESULT;
}

function SubClassFactory( FUNC )
{
	const CACHE = new Map();

	function SubClassFactoryInstance( TYPE, ...ARGS )
	{
		return Lookup( CACHE, TYPE, FUNC, ARGS );
	}
	return SubClassFactoryInstance;
}

module.exports = SubClassFactory;
///////////////////////////////////////////////////////////////////////////////////////////////////
