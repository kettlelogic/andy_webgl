
function isArray(x)
{
	return Array.isArray(x);
}

function isBuffer(x)            { return x === ArrayBuffer;  }
function isDataView(x)          { return x === DataView;     }
function isUint8Array(x)        { return x === Uint8Array;   }
function isUint16Array(x)       { return x === Uint16Array;  }
function isUint32Array(x)       { return x === Uint32Array;  }
function isInt8Array(x)         { return x === Int8Array;    }
function isInt32Array(x)        { return x === Int32Array;   }
function isInt16Array(x)        { return x === Int16Array;   }
function isUint8ClampedArray(x) { return x === Uint8ClampedArray; }
function isFloat32Array(x)      { return x === Float32Array; }
function isFloat64Array(x)      { return x === Float64Array; }
function isTypedArray(x)
{
	return	x === Float32Array ||
			x === Float64Array ||
			x === Int16Array   ||
			x === Int32Array   ||
			x === Int8Array    ||
			x === Uint16Array  ||
			x === Uint32Array  ||
			x === Uint8Array   ||
		 	x === Float32Array;
}
function isBufferView( x )
{
	return isDataView( x ) || isTypedArray( x );
}

function isBufferInstance(x)             { return ArrayBuffer.isPrototypeOf(x);       }
function isDataViewInstance(x)           { return DataView.isPrototypeOf(x);          }
function isUint8ArrayInstance(x)         { return Uint8Array.isPrototypeOf(x);        }
function isUint16ArrayInstance(x)        { return Uint16Array.isPrototypeOf(x);       }
function isUint32ArrayInstance(x)        { return Uint32Array.isPrototypeOf(x);       }
function isInt8ArrayInstance(x)          { return Int8Array.isPrototypeOf(x);         }
function isInt32ArraYInstance(x)         { return Int32Array.isPrototypeOf(x);        }
function isInt16ArrayInstance(x)         { return Int16Arra.isPrototypeOf(x);         }
function isUint8ClampedArrayInstance(x)  { return Uint8ClampedArray.isPrototypeOf(x); }
function isFloat32ArrayInstance(x)       { return Float32Arra.isPrototypeOf(x);       }
function isFloat64ArrayInstance(x)       { return Float64Array.isPrototypeOf(x);      }
function isTypedArrayInstance(x)
{
	return 	ArrayBuffer.isView(x) && !isDataViewInstance(x);
}
function isBufferViewInstance( x )
{
	return 	ArrayBuffer.isView(x);
}




module.exports =
{
	isArray,
	isBuffer,
	isDataView,
	isUint8Array,
	isUint16Array,
	isUint32Array,
	isInt8Array,
	isInt32Array,
	isInt16Array,
	isUint8ClampedArray,
	isFloat32Array,
	isFloat64Array,
	isTypedArray,
	isBufferView,
	isBufferInstance,
	isDataViewInstance,
	isUint8ArrayInstance,
	isUint16ArrayInstance,
	isUint32ArrayInstance,
	isInt8ArrayInstance,
	isInt32ArraYInstance,
	isInt16ArrayInstance,
	isUint8ClampedArrayInstance,
	isFloat32ArrayInstance,
	isFloat64ArrayInstance,
	isTypedArrayInstance,
	isBufferViewInstance
};