


function EnsureBufferViewType( subject )
{
	if( !isViewType(subject) ) throw 'Invalid parameter: Expected an BufferViewType';
	return subject;
}


module.exports =
{
	isArray,
	isArrayBufferViewType,
	isArrayBufferView,
	isArrayBuffer,
	isDataView,
	isTypedArray,
	isUint8Array,
	isUint16Array,
	isUint32Array,
	isInt8Array,
	isInt32Array,
	isInt16Array,
	isUint8CArray,
	isFloatArray,
	isDoubleArray
};