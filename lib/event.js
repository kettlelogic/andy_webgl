class Emitter
{
	constructor()
	{
		this.listeners = new Map();
	}
	on( key, callback, entry )
	{
		if( !( entry = this.listeners.get( key ) ) )
		{
			this.listeners.set( key, (entry = new Array()) );
		}
		if( entry.indexOf( callback ) === -1 )
		{
			entry.push( callback );
		}
		return this;
	}
	has( key, callback, entry )
	{
		return !!( entry = this.listeners.get( key ) ) && ( entry.indexOf( listener ) === -1 );
	}
	clear( key, callback, entry, index )
	{
		if( !!( entry = this.listeners.get( key ) ) && ( ( index = entry.indexOf( listener ) ) === -1 ) )
		{
			entry.splice( index, 1 );
		}
		return this;
	}
	emit( key, ...args )
	{
		let entry;
		if( !!( entry = this.listeners.get( key ) ) )
		{
			let l = entry.length
			for( let i = 0; i < l; i++ )
			{
				entry[ i ]( ...args );
			}
		}
	}
}
////////////////////////////////////////////////////////////////////////////////
function throttle( threshhold, fn, context=null )
{
	let last, deferTimer;
	return function( ...args )
	{
		let now = +new Date();
		if( last && (now < (last + threshhold)) )
		{
			clearTimeout( deferTimer );
			deferTimer = setTimeout( ()=>{ last = now; fn.apply( context, args ); }, threshhold );
		}
		else
		{
			last = now;
			fn.apply( context, args );
		}
	};
}
////////////////////////////////////////////////////////////////////////////////
function debounce( delay, fn, context=null )
{
	let timer = null;
	return function( ...args )
	{
		clearTimeout( timer );
		timer = setTimeout( ()=>(fn.apply( context, args )), delay );
	};
}
////////////////////////////////////////////////////////////////////////////////
module.exports = {
	Emitter,
	throttle,
	debounce
};
