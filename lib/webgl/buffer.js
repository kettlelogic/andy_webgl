require( './globals' );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const _usage =
{
	static :
	{
		copy : WEBGL2.STATIC_COPY,
		draw : WEBGL2.STATIC_DRAW,
		read : WEBGL2.STATIC_READ
	},
	dynamic :
	{
		copy : WEBGL2.DYNAMIC_COPY,
		draw : WEBGL2.DYNAMIC_DRAW,
		read : WEBGL2.DYNAMIC_READ
	},
	stream :
	{
		copy : WEBGL2.STREAM_COPY,
		draw : WEBGL2.STREAM_DRAW,
		read : WEBGL2.STREAM_READ
	}
};

const _target =
{

	array    : WEBGL2.ARRAY_BUFFER,
	elements : WEBGL2.ELEMENT_ARRAY_BUFFER,
	copy :
	{
		read  : WEBGL2.COPY_READ_BUFFER,
		write : WEBGL2.COPY_WRITE_BUFFER
	},
	transformFeedback : WEBGL2.TRANSFORM_FEEDBACK_BUFFER,
	uniform           : WEBGL2.UNIFORM_BUFFER,
	pixel :
	{
		pack   : WEBGL2.PIXEL_PACK_BUFFER,
		unpack : WEBGL2.PIXEL_UNPACK_BUFFER
	}
};

class GlBuffer
{
	////////////////////////////////////////////////////////////////
	static get usage()  { return _usage;  }
	static get target() { return _target; }

	static get static()   { return _usage.static;    }
	static get dynamic()  { return _usage.dynamic;   }
	static get stream()   { return _usage.stream;    }
	static get verticies(){ return _target.array;    }
	static get indicies() { return _target.elements; }
	static get copy()     { return _target.copy;     }


	////////////////////////////////////////////////////////////////
	constructor( target=_target.array, usage=_usage.static.draw, data, offset, length )
	{
		this.target = target;
		this.usage  = usage;
		this.ref    = gl.createBuffer();
		this.bound  = false;
	}
	bind()
	{
		if( !this.bound )
		{
			gl.bindBuffer( this.target, this.ref );
			this.bound = true;
		}
		return this;
	}
	unbind()
	{
		if( this.bound )
		{
			gl.bindBuffer( this.target, null );
			this.bound = false;
		}
		return this;
	}
	delete()
	{
		gl.deleteBuffer( this.ref );
		return this;
	}
/*
	// WebGL1:
	void gl.bufferData(target, size, usage);
	void gl.bufferData(target, ArrayBuffer? srcData, usage);
	void gl.bufferData(target, ArrayBufferView srcData, usage);
	// WebGL2:
	void gl.bufferData(target, ArrayBufferView srcData, usage, srcOffset, length);
*/
	data( data )
	{
		gl.bufferData( this.target, data, this.usage );
		return this;
	}
/*
	// WebGL1:
	void gl.bufferSubData(target, offset, ArrayBuffer srcData);
	void gl.bufferSubData(target, offset, ArrayBufferView srcData);
	// WebGL2:
	void gl.bufferSubData(target, dstByteOffset, ArrayBufferView srcData, srcOffset, length);
*/
	subData( offset, data )
	{
		gl.bufferSubData( this.target, offset, data );
		return this;
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
module.exports = GlBuffer;
