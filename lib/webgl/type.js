require( './globals' );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const { Composite, Handle, Primitive, Type, View } = require( '../buffer/type' );
const Attribute = require( './attribute' );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class GLPrimitive extends Primitive
{
	constructor( glenum, array_type, getter, setter )
	{
		super( array_type, getter, setter );
		this.glenum = glenum;
	}
	get isGLPrimitive()
	{
		return true;
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class GLView extends View
{
	constructor( glenum, type, normalize=false )
	{
		super( type );
		this.glenum    = glenum;
		this.normalize = normalize;
	}
	get isGLView()
	{
		return true;
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class GLComposite extends Composite
{
	constructor( members, normalize=false )
	{
		super( members, false );
		this._attributes = [];
		this.normalize = normalize;

		Object.defineProperty( this.handle, 'enableGlAttributes',
		{
			value : function () { this.typeinfo.enable(); }
		} );
		Object.defineProperty( this.handle, 'disableGlAttributes',
		{
			value : function () { this.typeinfo.disable(); }
		} );

		return this.handle;
	}
	enable()
	{
		this.attributes.forEach( (a)=>(a.enable()) );
	}
	disable()
	{
		this.attributes.forEach( (a)=>(a.disable()) );
	}
	get attributes()
	{
		if( this._attributes.length === 0 )
		{
			GenGLAttribute( this );
		}
		return this._attributes;
	}

	get isGLComposite()
	{
		return true;
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function GenGLAttribute( gltype, id_chain='', attribs=gltype._attributes, stride=gltype.size  )
{
	let field, type, id, len, glenum, norm, size, off;

	for( let key of gltype.keys )
	{
		field  = gltype.layout[key];
		type   = field.type;
		id     = `${id_chain}${field.key}`;
		len    = field.length;
		glenum = type.glenum;
		norm   = !!type.normalize;
		size   = type.size;
		off    = field.offset;
		if( !!field.isPrimitive )
		{
			attribs.push(new Attribute( id, len, glenum, norm, stride, off ));
			continue;
		}
		if( !!field.isView      )
		{
			attribs.push(new Attribute( id, len, glenum, norm, stride, off ));
			continue;
		}
		if( !!field.isComposite && field.isUniform )
		{
			attribs.push(new Attribute( id, len, type.uniType.glenum, norm, stride, off ));
			continue;
		}
		if( !!field.isComposite && !field.isUniform )
		{
			GenGLAttribute( type, `${id}_`, attribs, stride );
			continue;
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
const uint8	 = new GLPrimitive( WEBGL2.UNSIGNED_BYTE,  Uint8Array,   DataView.prototype.getUint8,   DataView.prototype.setUint8   );
const uint16 = new GLPrimitive( WEBGL2.UNSIGNED_SHORT, Uint16Array,  DataView.prototype.getUint16,  DataView.prototype.setUint16  );
const uint32 = new GLPrimitive( WEBGL2.UNSIGNED_INT,   Uint32Array,  DataView.prototype.getUint32,  DataView.prototype.setUint32  );
const int8	 = new GLPrimitive( WEBGL2.BYTE,           Int8Array,    DataView.prototype.getInt8,    DataView.prototype.setInt8    );
const int16	 = new GLPrimitive( WEBGL2.SHORT,          Int16Array,   DataView.prototype.getInt16,   DataView.prototype.setInt16   );
const int32	 = new GLPrimitive( WEBGL2.INT,            Int32Array,   DataView.prototype.getInt32,   DataView.prototype.setInt32   );
const float	 = new GLPrimitive( WEBGL2.FLOAT,          Float32Array, DataView.prototype.getFloat32, DataView.prototype.setFloat32 );
const uint   = uint32;
const int    = int32;
const byte   = int8;
const ubyte  = uint8;
const ushort = uint16;
const short  = int16;

const { Vector2, Vector3, Vector4, Quaternion, Matrix2, Matrix2d, Matrix3, Matrix4 } = require( '../math/matrix');
const { RGBA } = require( '../math/color' );

const v2   = new GLView( WEBGL2.FLOAT,         Vector2  );
const v3   = new GLView( WEBGL2.FLOAT,         Vector3  );
const v4   = new GLView( WEBGL2.FLOAT,         Vector4  );
const m2   = new GLView( WEBGL2.FLOAT,         Matrix2  );
const m2d  = new GLView( WEBGL2.FLOAT,         Matrix2d );
const m3   = new GLView( WEBGL2.FLOAT,         Matrix3  );
const m4   = new GLView( WEBGL2.FLOAT,         Matrix4  );
const rgba = new GLView( WEBGL2.UNSIGNED_BYTE, RGBA     , true );

module.exports =
{
	Primitive : GLPrimitive,
	View      : GLView,
	Composite : GLComposite,

	uint8 	  : uint8,
	uint16 	  : uint16,
	uint32 	  : uint32,
	int8 	  : int8,
	int16 	  : int16,
	int32 	  : int32,
	float 	  : float,
	uint 	  : uint32,
	int 	  : int32,
	ubyte 	  : uint8,
	byte 	  : int8,

	Vector2   : v2,
	Vector3   : v3,
	Vector4   : v4,
	Matrix2   : m2,
	Matrix2d  : m2d,
	Matrix3   : m3,
	Matrix4   : m4,

	RGBA      : rgba
};
