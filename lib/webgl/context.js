require( './globals' );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

const _draw_mode =
{
	points    : WEBGL2.POINTS,
	lines     : WEBGL2.LINES,
	line      :
	{
		loop  : WEBGL2.LINE_LOOP,
		strip : WEBGL2.LINE_STRIP
	},
	triangles : WEBGL2.TRIANGLES,
	triangle  :
	{
		strip : WEBGL2.TRIANGLE_STRIP,
		fan   : WEBGL2.TRIANGLE_FAN
	}
};

const _contexts_ = new WeakMap();

class Context
{
	static get mode()
	{
		return _draw_mode;
	}
	static get current()
	{
		return window.gl;
	}
	static set current( context )
	{
		window.gl = context;
	}
	static For( cavnas )
	{
		return _contexts_.get( canvas );
	}
	////////////////////////////////////////////////////////////////
	constructor( canvas = document.createElement('canvas') )
	{
		this.canvas      = canvas;
		this.gl          = canvas.getContext("webgl2")
		_contexts_.set( canvas, this );
		this.clearbits   = 0;
	}
	makeCurrent()
	{
		Context.current = _contexts_.get( this.canvas ).gl;
		return this;
	}

	// CANVAS OPERATION ////////////////////////////////////////////
	get id()        { return this.canvas.id; }
	get element()   { return this.canvas; }
	get width()     { return this.canvas.offsetWidth; }
	get height()    { return this.canvas.offsetHeight; }
	get size()      { return [this.width, this.height]; }
	set id(v)       { canvas.id = v; }
	set width(v)
	{
		this.canvas.width = v;
		gl.viewport(0, 0, v, this.height);
	}
	set height(v)
	{
		this.canvas.height = v;
		gl.viewport(0, 0, this.width, v);
	}
	set size(v) { this.resize(...v); }
	resize( width, height )
	{
		[this.canvas.width, this.canvas.height] = [width, height];
		this.viewport(0, 0, width, height);
		return this;
	}

	// OPENGL OPERATIONS ///////////////////////////////////////////
	clearDepth( bool, depth=1 )
	{
		if( bool )
		{
			gl.enable( gl.DEPTH_TEST );
			gl.clearDepth( depth );
			this.clearbits |= gl.DEPTH_BUFFER_BIT;
			return this;
		}
		else
		{
			gl.disable( gl.DEPTH_TEST );
			this.clearbits &= ~(1 << gl.DEPTH_BUFFER_BIT);
		}
		return this;
	}
	clearColor(r,g,b,a)
	{
		this.clearbits   |= gl.COLOR_BUFFER_BIT;
		gl.clearColor(r,g,b,a);
		return this;
	}
	clear()
	{
		gl.clear( this.clearbits );
		return this;
	}
	cull( mode )
	{
		gl.enable( gl.CULL_FACE );

		return this;
	}
	blend( bool )
	{
		if( bool )
		{
			gl.enable( gl.BLEND );
		}
		else
		{
			gl.disable( gl.BLEND );
		}
		return this;
	}
	viewport( x,y, w,h )
	{
		//this.uResolution.set( [w,h] )
		gl.viewport( x,y, w,h );
		return this;
	}
	draw( mode, first, count )
	{
		gl.drawArrays( mode, first, count );
	}
	drawElements( mode, count, type, offset )
	{
		gl.drawElements( mode, count, type, offset )
	}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

module.exports = Context;
