const Program = require( './program' );
const base = require( '../math/matrix');

class Location
{
	constructor( program, uniform )
	{
		this.pid = program.id;
		this.id  = gl.getUniformLocation( program.id,`u_${uniform.name}` );
	}
}
function ExtendValueType( func )
{
	return class Value
	{
		constructor( name, value )
		{
			this.locations = new WeakMap();
			this.location  = null;
			this.name      = name;
			this.func      = func;
			this.value     = value;
		}
		set( value )
		{
			this.value = value;
		}
		bind( program=Program.current )
		{
			if( Program.current === null )
				throw 'No Program';
			if( !(this.location = this.locations.get( program )) )
				this.locations.set( program, (this.location = new Location( program, this )) );
			return this;
		}
		enable()
		{
			this.bind();
			this.func.call( gl, this.location.id, this.value );
			return this;
		}
		disable()
		{
			return this;
		}
	}
}
function ExtendArrayType( base, func )
{
	return class Uniform extends base
	{
		constructor( name, ...args )
		{
			super( ...args );
			this.locations = new WeakMap();
			this.location  = null;
			this.name      = name;
			this.func      = func;
		}
		bind( program=Program.current )
		{
			if( Program.current === null )
				throw 'No Program';
			if( !(this.location = this.locations.get( program )) )
				this.locations.set( program, (this.location = new Location( program, this )) );
			return this;
		}
		enable()
		{
			this.bind();
			if( this.isVector )
			{
				this.func.call( gl, this.location.id, this );
			}
			else
			{
				this.func.call( gl, this.location.id, false, this );
			}
			return this;
		}
		disable()
		{
			return this;
		}
	}
}
const Integer = ExtendValueType( WEBGL2.uniform1i );
const Float   = ExtendValueType( WEBGL2.uniform1f );

const Vector2 = ExtendArrayType( base.Vector2, WEBGL2.uniform2fv       );
const Vector3 = ExtendArrayType( base.Vector3, WEBGL2.uniform3fv       );
const Vector4 = ExtendArrayType( base.Vector4, WEBGL2.uniform4fv       );
const Matrix2 = ExtendArrayType( base.Matrix2, WEBGL2.uniformMatrix2fv );
const Matrix3 = ExtendArrayType( base.Matrix3, WEBGL2.uniformMatrix3fv );
const Matrix4 = ExtendArrayType( base.Matrix4, WEBGL2.uniformMatrix4fv );

module.exports =
{
	Integer,
	Float,
	Vector2,
	Vector3,
	Vector4,
	Matrix2,
	Matrix3,
	Matrix4,
};


/*
void gl.uniformMatrix2fv(location, transpose, data, optional srcOffset, optional srcLength);
void gl.uniformMatrix3x2fv(location, transpose, data, optional srcOffset, optional srcLength);
void gl.uniformMatrix4x2fv(location, transpose, data, optional srcOffset, optional srcLength);
void gl.uniformMatrix2x3fv(location, transpose, data, optional srcOffset, optional srcLength);
void gl.uniformMatrix3fv(location, transpose, data, optional srcOffset, optional srcLength);
void gl.uniformMatrix4x3fv(location, transpose, data, optional srcOffset, optional srcLength);
void gl.uniformMatrix2x4fv(location, transpose, data, optional srcOffset, optional srcLength);
void gl.uniformMatrix3x4fv(location, transpose, data, optional srcOffset, optional srcLength);
void gl.uniformMatrix4fv(location, transpose, data, optional srcOffset, optional srcLength);
*/
