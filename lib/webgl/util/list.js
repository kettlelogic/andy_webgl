class Node {
    constructor(list, item, next, prev) {
        this.list = list;
        this.next = next;
        this.prev = prev;
        this.item = item;
    }
}

class List {
    constructor(...items) {
        this.length = 0;
        this.head = null;
        this.tail = null;

        items.forEach((i) => (this.push(i)));
    }

    get empty() {
        return this.length === 0;
    }

    push(item) {
        if (!this.empty) this.tail = this.tail.next = new Node(this, item, null, this.tail);
        else this.head = this.tail = new Node(this, item, null, null);
        this.length++;
        return this;
    }
    unshift(item) {
        if (!this.empty) this.head = this.head.prev = new Node(this, item, this.head, null);
        else this.head = this.tail = new Node(this, item, null, null);
        this.length++;
        return this;
    }
    pop(item) {
        return this.remove(this.tail);
    }
    shift(item) {
        return this.remove(this.head);
    }
    remove(item) {
        let node = this.head;
        while (node) {
            if (node.item !== item) {
                node = node.next;
                continue
            };
            return this.removeNode(node).item;
        }
        return false;
    }
    removeNode(n) {
        if (n === this.head) this.head = n.next;
        if (n === this.tail) this.tail = n.prev;
        if (n.next) n.next.prev = n.prev;
        if (n.prev) n.prev.next = n.next;

        this.length--;
        return n;
    }
    each(f) {
        let node = this.head;
        while (node) {
            f(node.item, node.prev.item, node.next.item);
            node = node.next;
        }
        return this;
    }
    eachFromTail(f) {
        let node = this.tail;
        while (node) {
            f(node.item, node.prev.item, node.next.item);
            node = node.prev;
        }
        return this;
    }
    contains(item) {
        let node = this.head;
        while (node) {
            if (node.item === item) return true;
        }
        return false;
    }
    nodeOf(item) {
        let node = this.head;
        while (node) {
            if (node.item === item) return node;
        }
        return null;
    }
    rotate(item) {

    }
}

module.exports = List;
