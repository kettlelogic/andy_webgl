
if( window.gl === undefined ) window.gl = null;

const _contexts_ = new WeakMap();

class ContextManagement
{
	static get( canvas, gl )
	{
		if( !(gl = _contexts_.get( canvas )) )
			_contexts_.set( canvas, (gl = canvas.getContext("webgl2")) );
		return gl;
	}
	static set( canvas )
	{
		window.gl = ContextManagement.get( canvas );
	}
}

module.exports = ContextManagement;