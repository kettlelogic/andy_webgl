require( './globals' );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class Location
{
	constructor( program, attribute )
	{
		this.pid   = program.id;
		this.index = gl.getAttribLocation( program.id, `a_${attribute.name}` );
		if( this.index === -1 )
			throw `invalid attribute index for a_${attribute.name}`;
	}
}
class Attribute
{
	constructor( name, size, type, normalize, stride, offset )
	{
		this.locations = new WeakMap();
		this.location  = null;
		this.name      = name;
		this.size      = size;
		this.type      = type;
		this.normalize = normalize;
		this.stride    = stride;
		this.offset    = offset;
	}
	bind( program=Program.current )
	{
		if( Program.current === null )
			throw 'No Program';
		if( !(this.location = this.locations.get( program )) )
				this.locations.set( program, (this.location = new Location( program, this )) );
		return this;
	}
	enable()
	{
		this.bind();
		gl.vertexAttribPointer( this.location.index, (this.size)|0, this.type, this.normalize, (this.stride)|0, (this.offset)|0 );
		gl.enableVertexAttribArray( this.location.index );
		return this;
	}
	toString()
	{
		return `[Attribute]{ name: a_${this.name}, index:${this.location.index}, size:${this.size}, type:${this.type}, normalize:${this.normalize}, stride:${this.stride}, offset:${this.offset} }`
	}
	disable()
	{
		gl.disableVertexAttribArray( this.location.index );
		return this;
	}
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
module.exports = Attribute;