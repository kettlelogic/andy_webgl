require( './globals' );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

let _current_ = null;

class Program
{
	static get current()
	{
		return _current_;
	}
	////////////////////////////////////////////////////////////////

	constructor( vshader, fshader )//, uniforms )// attributes, uniforms )
	{
		this.id         = gl.createProgram();
		this.vert       = vshader;
		this.frag       = fshader;
		this.ready      = false;
		this.error      = false;
		//this.attributes = attributes;
		//this.uniforms   = uniforms;
		this.enabled    = false;

		this.uniform = {
			resolution:
			{
				id: null,
				value: null
			},
			scale:
			{
				id: null,
				value: null
			}
		};

		gl.attachShader( this.id, vshader.id );
		gl.attachShader( this.id, fshader.id );

		if( this.link() )
		{
			//this.attributes.forEach( ( a ) => ( a.bind( this ) ) );
			//this.uniforms.forEach(   ( u ) => ( u.bind( this ) ) );
		}
	}

	link()
	{
		gl.linkProgram( this.id );
		if( !this.linked )
		{
			console.log( this.info );
			return !( this.error = true );
		}
		return( this.ready = true );
	}
	enable()
	{
		if( _current_ !== this )
		{
			if( _current_ !== null ) _current_.disable();
			_current_ = this;
		}
		if( !this.enabled )
		{
			gl.useProgram( this.id );
			//this.attributes.forEach( ( a ) => ( a.bind( this ).enable() ) );
			//this.uniforms.forEach(   ( u ) => ( u.bind( this ).enable() ) );
			this.enabled = true;
		}
		return this;
	}
	disable()
	{
		if( _current_ !== null && _current_ === this )
		{
			_current_ = null;
		}
		if( this.enabled )
		{
			//this.attributes.forEach( ( a ) => ( a.disable() ) );
			//this.uniforms.forEach(   ( u ) => ( u.disable() ) );
			gl.useProgram( null );
			this.enabled = false;
		}
		return this;
	}

	get attributeCount()
	{
		return gl.getProgramParameter( this.id, gl.ACTIVE_ATTRIBUTES );
	}
	get uniformCount()
	{
		return gl.getProgramParameter( this.id, gl.ACTIVE_UNIFORMS );
	}
	get attributes()
	{
		let num = this.attributeCount;
		let r = new Array( num );

		for( let i = 0; i<num; i++)
		{
			r[i] = gl.getActiveAttrib( this.id, i );
		}
		return r;
	}
	get uniforms()
	{
		let num = this.uniformCount;
		let r = new Array( num );

		for( let i = 0; i<num; i++)
		{
			r[i] = gl.getActiveUniform( this.id, i );
		}
		return r;
	}

	get linked()
	{
		return gl.getProgramParameter( this.id, gl.LINK_STATUS );
	}
	get info()
	{
		return gl.getProgramInfoLog( this.id );
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
module.exports = Program;
