require( './globals' );
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class Shader
{
	////////////////////////////////////////////////////////////////
	static get vertex()   { return gl.VERTEX_SHADER; }
	static get fragment() { return gl.FRAGMENT_SHADER; }
	////////////////////////////////////////////////////////////////

	constructor( type, source )
	{
		this.id    = gl.createShader( type );
		this.error = false;
		this.ready = false;
		this.compile( source );
	}
	compile( source )
	{
		gl.shaderSource( this.id, source );
		gl.compileShader( this.id );

		if( !this.compiled )
		{
			this.error = true;
			console.log( this.info );
			return this;
		}
		this.ready = true;
		return this;
	}
	get compiled()
	{
		return gl.getShaderParameter( this.id, gl.COMPILE_STATUS );
	}
	get info()
	{
		return gl.getShaderInfoLog( this.id );
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
module.exports = Shader;
