

ArrayBuffer.prototype.copy = function( dst, dst_offset, src, src_offset, length )
{
	let s = Uint8Array( src, src_offset, length );
	let d = Uint8Array( dst, dst_offset, length );

	for( let i=0; i<length; i++ ) d[i] = s[i];

	return this;
}
ArrayBuffer.of( ...types )
{
	let data   = new ArrayBuffer( types.reduce( (total,T)=>(total+T.BYTES) ) );
	let offset = 0;
	let T      = null;

	for( let i=0; i<types.length; i++ )
	{
		T        = types[i];
		types[i] = new T( data, offset, T.BYTES );
		offset   = offset + T.BYTES;
	}
	return types;
}




