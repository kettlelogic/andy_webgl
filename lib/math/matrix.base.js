function parse_args( length, args )
{
	return args.length ? [...args,length] : [length];
}

function Matrix_Template( base, length )
{
	class Matrix extends Float32Array
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		static get idenity()
		{
			return base.idenity( new this.constructor() )
		}
		static get ELEMENTS()
		{
			return length;
		}
		static get BYTE_LENGTH()
		{
			return length * Float32Array.BYTES_PER_ELEMENT;
		}
		static get BYTES()
		{
			return length * Float32Array.BYTES_PER_ELEMENT;
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		static clone( m )
		{
			return new this.constructor( m );
		}
		static invert( m, out=(new this.constructor()) )
		{
			return base.invert( out, m );
		}
		static determinant( m )
		{
			return base.determinant( m );
		}
		static multiply( a,b, out=(new this.constructor()) )
		{
			return base.multiply( out, a, b );
		}
		static mul( a,b, out=(new this.constructor()) )
		{
			return base.mul( out, a, b  );
		}
		static rotate( m, radians, out=(new this.constructor()) )
		{
			return base.rotate( out, m, radians );
		}
		static scale( m, vec2, out=(new this.constructor()) )
		{
			return base.scale( out, m, vec2 );
		}
		static fromRotation( radians, out=(new this.constructor()) )
		{
			return base.fromRotation( out, radians );
		}
		static fromScaling( factor, out=(new this.constructor()) )
		{
			return base.fromScaling( out, factor );
		}
		static str( m )
		{
			return base.str( m );
		}
		static frob( m )
		{
			return base.frob( m );
		}
		static exactEquals( a, b )
		{
			return base.exactEquals( a, b );
		}
		static equals( a, b )
		{
			return base.equals( a, b );
		}
		static multiplyScalar( m, factor, out=(new this.constructor()) )
		{
			return base.multiplyScalar( out, m, factor );
		}
		static multiplyScalarAndAdd( a, b, factor, out=(new this.constructor()) )
		{
			return base.multiplyScalarAndAdd( out, a, b, factor );
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		constructor( ...args )
		{
			super( ...(parse_args( length, args )) );
			base.identity( this );
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		get isMatrix()
		{
			return true;
		}
		get str()
		{
			return base.str( this );
		}
		get frob()
		{
			return base.frob( this );
		}
		get clone()
		{
			return new this.constructor( this );
		}
		get determinant()
		{
			return base.determinant( this );
		}
		get inverse()
		{
			return base.invert( out, this );
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		copy( other )
		{
			return base.copy( this, other );
		}
		setToIdentity()
		{
			return base.identity( this );
		}
		set( ...args )
		{
			return base.set( this, ...args );
		}
		invert()
		{
			return base.invert( this, this );
		}
		multiply( other )
		{
			return base.multiply( this, this, other );
		}
		mul( other )
		{
			return base.mul( this, this, other );
		}
		rotate( radians )
		{
			return base.rotate( this, this, radians );
		}
		scale( vec2 )
		{
			return base.scale( this, this, vec2 );
		}
		add( other )
		{
			return base.add( this, this, other );
		}
		subtract( other )
		{
			return base.subtract( this, this, other );
		}
		sub( other )
		{
			return base.subtract( this, this, other );
		}
		exactEquals( other )
		{
			return base.exactEquals( this, other );
		}
		equals( other )
		{
			return base.equals( this, other );
		}
		multiplyScalar( factor )
		{
			return base.multiplyScalar( this, this, factor );
		}
		multiplyScalarAndAdd( other, factor )
		{
			return base.multiplyScalarAndAdd( this, this, other, factor );
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	}
	return Matrix;
}
module.exports = {Matrix_Template};