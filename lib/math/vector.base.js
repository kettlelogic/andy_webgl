function parse_args( length, args )
{
	return args.length ? [...args,length] : [length];
}

function Vector_Template( base, length )
{
	class Vector extends Float32Array
	{
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		static get ELEMENTS()
		{
			return length;
		}
		static get BYTE_LENGTH()
		{
			return length * Float32Array.BYTES_PER_ELEMENT;
		}
		static get BYTES()
		{
			return length * Float32Array.BYTES_PER_ELEMENT;
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		static clone( a )
		{
			return new this.constructor( a );
		}
		static add( a, b, out=(new this.constructor()) )
		{
			return base.add( out, a, b );
		}
		static sub( a, b, out=(new this.constructor()) )
		{
			return base.subtract( out, a, b );
		}
		static subtract( a, b, out=(new this.constructor()) )
		{
			return base.subtract( out, a, b );
		}
		static mul( a, b, out=(new this.constructor()) )
		{
			return base.multiply( out, a, b );
		}
		static multiply( a, b, out=(new this.constructor()) )
		{
			return base.multiply( out, a, b );
		}
		static div( a, b, out=(new this.constructor()) )
		{
			return base.divide( out, a, b );
		}
		static divide( a, b, out=(new this.constructor()) )
		{
			return base.divide( out, a, b );
		}
		static ceil( a, out=(new this.constructor()) )
		{
			return base.ciel( out, a );
		}
		static floor( a, out=(new this.constructor()) )
		{
			return base.floor( out, a );
		}
		static round( a, out=(new this.constructor()) )
		{
			return base.round( out, a );
		}
		static min( a, b, out=(new this.constructor()) )
		{
			return base.min( out, a, b );
		}
		static max( a, b, out=(new this.constructor()) )
		{
			return base.max( out, a, b );
		}
		static each( array, stride, offset, count, fn, arg )
		{
			return base.each( array, stride, offset, count, fn, arg );
		}
		static scale( a, factor, out=(new this.constructor()) )
		{
			return base.scale( out, a, factor );
		}
		static scaleAndAdd( a, b, factor, out=(new this.constructor()) )
		{
			return base.scaleAndAdd( out, a, b, factor );
		}
		static distance( a, b, out=(new this.constructor()) )
		{
			return base.distance( out, b );
		}
		static dist( a, b, out=(new this.constructor()) )
		{
			return base.distance( out, b );
		}
		static squaredDistance( a, b )
		{
			return base.squaredDistance( a, b );
		}
		static sqrDist( a, b )
		{
			return base.squaredDistance( b );
		}
		static length( a )
		{
			return base.length( a );
		}
		static len( a )
		{
			return base.length( a );
		}
		static squaredLength( a )
		{
			return base.squaredLength( a );
		}
		static sqrLen( a )
		{
			return base.squaredLength();
		}
		static negate( a, out=(new this.constructor()) )
		{
			return base.negate( out, a );
		}
		static inverse( out=(new this.constructor()) )
		{
			return base.inverse( out, a );
		}
		static normalize( a, out=(new this.constructor()) )
		{
			return base.normalize( out, a );
		}
		static dot( a, b )
		{
			return base.dot( a, b );
		}
		static lerp( a, b, factor )
		{
			return base.lerp( out, a, b, factor );
		}
		static random( factor = 1.0, out=(new this.constructor()) )
		{
			return base.random( out, factor );
		}
		static transformMat4( a, mat4, out=(new this.constructor()) )
		{
			return base.transformMat4( out, a, mat4 );
		}
		static toString( a )
		{
			return base.str( a );
		}
		static exactEquals( a, b )
		{
			return base.exactEquals( a, b );
		}
		static equals( a, b )
		{
			return base.equals( a, b );
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		constructor( ...args )
		{
			super( ...(parse_args( length, args )) );
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		get isVector()
		{
			return true;
		}
		get clone()
		{
			return new this.constructor( this );
		}
		get magnitude()
		{
			return base.length( this );
		}
		get mag()
		{
			return base.length( this );
		}
		get squaredLength()
		{
			return base.squaredLength( this );
		}
		get sqrLen()
		{
			return base.squaredLength( this );
		}
		get negation()
		{
			return base.negate( this.clone, this );
		}
		get inverse()
		{
			return base.inverse( this.clone, this );
		}
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		copy( other )
		{
			return base.set( this, ...other );
		}
		set( ...args )
		{
			return base.set( this, ...args );
		}
		add( other )
		{
			return base.add( this, this, other );
		}
		subtract( other )
		{
			return base.subtract( this, this, other );
		}
		sub( other )
		{
			return this.subtract( other );
		}
		multiply( other )
		{
			return base.multiply( this, this, other );
		}
		mul()
		{
			return this.multiply( other );
		}
		divide( other )
		{
			return base.divide( this, this, other );
		}
		div()
		{
			return this.divide( other );
		}
		ceil()
		{
			return base.ceil( this, this );
		}
		floor()
		{
			return base.floor( this, this );
		}
		min()
		{
			return base.min( this, other );
		}
		max()
		{
			return base.max( this, other );
		}
		round()
		{
			return base.round( this, this );
		}
		scale( factor )
		{
			return base.scale( this, this, factor );
		}
		scaleAndAdd( other, factor )
		{
			return base.scaleAndAdd( this, this, other, factor );
		}
		distance( other )
		{
			return base.distance( this, other );
		}
		dist( other )
		{
			return this.distance( other );
		}
		squaredDistance( other )
		{
			return base.squaredDistance( this, other );
		}
		sqrDist( other )
		{
			return this.squaredDistance( other );
		}
		negate()
		{
			return base.negate( this, this );
		}
		invert()
		{
			return base.inverse( this, this );
		}
		normalize()
		{
			return base.normalize( this, this );
		}
		dot( other )
		{
			return base.dot( this, other );
		}
		lerp( other, factor )
		{
			return base.lerp( this, this, other, factor );
		}
		random( factor = 1.0 )
		{
			return base.random( this, factor );
		}
		transformMat4( mat4 )
		{
			return base.transformMat4( this, this, mat4 );
		}
		toString()
		{
			return base.str( this );
		}
		get [Symbol.toStringTag]()
		{
			return base.str( this );
		}
		exactEquals( other )
		{
			return base.exactEquals( this, other );
		}
		equals( other )
		{
			return base.equals( this, other );
		}
	}
	return Vector;
}

module.exports = {Vector_Template};