const { vec2            } = require( "./gl-matrix/index" );
const { Vector_Template } = require( "./vector.base"     );
const BaseClass = Vector_Template( vec2, 2 );

class Vector2 extends BaseClass
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	static cross( a, b )
	{
		return vec2.cross( new Vector2(), a, b );
	}
	static transformMat2( v, m )
	{
		return vec2.transformMat2( new Vector2(), v, m );
	}
	static transformMat2d( v, m )
	{
		return vec2.transformMat2d( new Vector2(), v, m );
	}
	static transformMat3( v, m )
	{
		return vec2.transformMat3( new Vector3(), v, m );
	}
	static fromObject( {x=0.0, y=0.0}, out=(new Vector2()) )
	{
		return vec2.set( out, x, y );
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	constructor( ...args )
	{
		super( ...args );
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	get isVector2()
	{
		return true;
	}
	get x()
	{
		return this[ 0 ];
	}
	get y()
	{
		return this[ 1 ];
	}
	set x( value )
	{
		this[ 0 ] = value;
	}
	set y( value )
	{
		this[ 1 ] = value;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	setFromObject( {x=0.0,y=0.0} )
	{
		return vec2.set( this, x, y );
	}
	cross( b )
	{
		return vec2.cross( this, this, b );
	}
	transformMat2( m )
	{
		return vec2.transformMat2( this, this, m );
	}
	transformMat2d( m )
	{
		return vec2.transformMat2d( this, this, m );
	}
	transformMat3( m )
	{
		return vec2.transformMat3( this, this, m );
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}


module.exports = {Vector2};