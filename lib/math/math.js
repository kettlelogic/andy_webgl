const { Vector2, Vector3, Vector4, Quaternion, Matrix2, Matrix2d, Matrix3, Matrix4 } = require( "./matrix" );
const { RGBA } = require( './color' );
////////////////////////////////////////////////////////////////////////////////
const E           = Math.E;
const LN10        = Math.LN10;
const LN2         = Math.LN2;
const LOG10E      = Math.LOG10E;
const LOG2E       = Math.LOG2E;
const PI          = Math.PI;
const SQRT1_2     = Math.SQRT1_2;
const SQRT2       = Math.SQRT2;
const TAU         = Math.PI * 2;
const HPI         = Math.PI / 2.0;
const deg_per_rad = 180 / PI;
const deg_per_tau = 360 / TAU;
const rad_per_deg = PI  / 180;
const tau_per_deg = TAU / 360;
////////////////////////////////////////////////////////////////////////////////
const abs         = Math.abs;
const acos        = Math.acos;
const acosh       = Math.acosh;
const asin        = Math.asin;
const asinh       = Math.asinh;
const atan        = Math.atan;
const atan2       = Math.atan2;
const atanh       = Math.atanh;
const cbrt        = Math.cbrt;
const ceil        = Math.ceil;
const clz32       = Math.clz32;
const cos         = Math.cos;
const cosh        = Math.cosh;
const exp         = Math.exp;
const expm1       = Math.expm1;
const floor       = Math.floor;
const fround      = Math.fround;
const hypot       = Math.hypot;
const imul        = Math.imul;
const log         = Math.log;
const log10       = Math.log10;
const log1p       = Math.log1p;
const log2        = Math.log2;
const max         = Math.max;
const min         = Math.min;
const pow         = Math.pow;
const rand        = Math.random;
const round       = Math.round;
const sign        = Math.sign;
const sin         = Math.sin;
const sinh        = Math.sinh;
const sqrt        = Math.sqrt;
const tan         = Math.tan;
const tanh        = Math.tanh;
const trunc       = Math.trunc;
////////////////////////////////////////////////////////////////////////////////

function clamp( lower,upper,value )
{
    return max( lower, min( value, upper ) );
}
function nearestIncrement( increment,value )
{
    return round(value / increment)*increment;
}
function scale( _old, _new, value )
{
    return value * _new / _old;
}

const deg2tau = ( v )=>( v * tau_per_deg );
const deg2rad = ( v )=>( v * rad_per_deg );
const rad2deg = ( v )=>( v * deg_per_rad );
const tau2deg = ( v )=>( v * tau_per_deg );

module.exports =
{
	E,LN10,LN2,LOG10E,LOG2E,PI,SQRT1_2,SQRT2,TAU,HPI,
	deg_per_rad,deg_per_tau,rad_per_deg,tau_per_deg,

	abs,acos,acosh,asin,asinh,atan,atan2,atanh,cbrt,
	ceil,clz32,cos,cosh,exp,expm1,floor,fround,hypot,
	imul,log,log10,log1p,log2,max,min,pow,rand,round,
	sign,sin,sinh,sqrt,tan,tanh,trunc,

	clamp,nearestIncrement,scale,deg2tau,deg2rad,
	rad2deg,tau2deg,

	Vector2,Vector3,Vector4,Quaternion,Matrix2,Matrix2d,
	Matrix3,Matrix4,

	RGBA
};