const { vec3            } = require( "./gl-matrix/index" );
const { Vector_Template } = require( "./vector.base"     );

class Vector3 extends Vector_Template( vec3, 3 )
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	static cross( a, b )
	{
		return vec3.cross( new Vector3(), a, b );
	}
	static hermite( v1, v2, v3, v4, factor )
	{
		return vec3.hermite( new Vector3(), v1, v2, v3, v4, factor );
	}
	static bezier( v1, v2, v3, v4, factor )
	{
		return vec3.bezier( new Vector3(), v1, v2, v3, v4, factor );
	}
	static transformMat3( v, m )
	{
		return vec3.transformMat3( new Vector3(), v, m );
	}
	static transformQuat( v, q )
	{
		return basetransformQuat( new Vector3(), v, q );
	}
	static rotateX( v, origin, angle )
	{
		return vec3.rotateX( new Vector3(), v, origin, angle );
	}
	static rotateY( v, origin, angle )
	{
		return vec3.rotateY( new Vector3(), v, origin, angle );
	}
	static rotateZ( v, origin, angle )
	{
		return vec3.rotateZ( new Vector3(), v, origin, angle );
	}
	static angle( a, b )
	{
		return vec3.angle( a, b );
	}
	static fromObject( {x=0.0, y=0.0, z=0.0}, out=(new Vector4()) )
	{
		return vec3.set( out, x, y, z );
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	get isVector3()
	{
		return true;
	}
	get x()
	{
		return this[ 0 ];
	}
	get y()
	{
		return this[ 1 ];
	}
	get z()
	{
		return this[ 2 ];
	}
	set x( value )
	{
		this[ 0 ] = value;
	}
	set y( value )
	{
		this[ 1 ] = value;
	}
	set z( value )
	{
		this[ 2 ] = value;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	setFromObject( {x=0.0, y=0.0, z=0.0} )
	{
		return vec3.set( this, x, y, z );
	}
	cross( b )
	{
		return vec3.cross( this, this, b );
	}
	transformMat3( m )
	{
		return vec3.transformMat3( this, this, m );
	}
	transformQuat( q )
	{
		return vec3.transformQuat( this, this, q );
	}
	rotateX( angle, origin=[0,0,0] )
	{
		return vec3.rotateX( this, this, origin, angle );
	}
	rotateY( angle, origin=[0,0,0] )
	{
		return vec3.rotateY( this, this, origin, angle );
	}
	rotateZ( angle, origin=[0,0,0] )
	{
		return vec3.rotateZ( this, this, origin, angle );
	}
	angle( other )
	{
		return vec3.angle( this, other );
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
module.exports = {Vector3};