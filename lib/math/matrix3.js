const { mat3            } = require( "./gl-matrix/index" );
const { Matrix_Template } = require( "./matrix.base"     );

class Matrix3 extends Matrix_Template( mat3, 9 )
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	static fromMat4( mat4 )
	{
		return mat3.fromMat4( new Matrix3(), mat4 );
	}
	static fromTranslation( vec2 )
	{
		return mat3.fromTranslation( new Matrix3(), vec2 );
	}
	static fromMat2d( mat2d )
	{
		return mat3.fromMat2d( new Matrix3(), mat2d );
	}
	static fromQuat( quat )
	{
		return mat3.fromQuat( new Matrix3(), quat );
	}
	static normalFromMat4( mat4 )
	{
		return mat3.normalFromMat4( new Matrix3(), mat4 );
	}
	static transpose( mat3 )
	{
		return mat3.transpose( new Matrix3(), mat3 );
	}
	static adjoint( mat3 )
	{
		return mat3.adjoint( new Matrix3(), mat3 );
	}
	static translate( mat3, vec2 )
	{
		return mat3.translate( new Matrix3(), mat3, vec2 );
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	constructor( ...args )
	{
		super( ...args );
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	get isMatrix3()
	{
		return true;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	transpose()
	{
		return mat3.transpose( this, this );
	}
	adjoint()
	{
		return mat3.adjoint( this, this );
	}
	translate( vec2 )
	{
		return mat3.translate( this, this, vec2 );
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
module.exports = {Matrix3};