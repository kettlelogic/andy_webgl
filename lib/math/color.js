
function parse_args( length, args )
{
	return args.length ? args : [length];
}
const pow     = Math.pow;
const sqrt    = Math.sqrt;
const abs     = Math.abs;
const isArray = Array.isArray;
const Cs      = (c      ) => (c / 255);
const RGB     = (c=Cs(c)) => (c <= 0.03928)?(c / 12.92):(pow(((c + 0.055) / 1.055), 2.4));

const BlendMode =
{
	multiply(c, other)
	{
        return c * other;
    },
    screen(c, other)
	{
        return c + other - c * other;
    },
    overlay(c=c*2, other, fn)
	{
        return (c <= 1) ? BlendMode.multiply(c, other) : BlendMode.screen(c - 1, other);
    },
    softlight(c, other)
	{
        let d = 1, e = c;
        if( other > 0.5 )
		{
            e = 1;
            d = (c > 0.25) ? sqrt(c) : (((16 * c - 12) * c + 4) * c);
        }
        return c - (1 - 2 * other) * e * (d - c);
    },
    hardlight(c, other)
	{
        return BlendMode.overlay(other, c);
    },
    difference(c, other)
	{
        return abs(c - other);
    },
    exclusion: function(c, other) {
        return c + other - 2 * c * other;
    },
    average(c, other)
	{
        return (c + other) / 2;
    },
    negation(c, other)
	{
        return 1 - abs(c + other - 1);
    }
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class RGBA extends Uint8Array
{

	static blend( a, b )
	{
		return a.clone.mix(b);
	}
	/////////////////////////////////////////////////////////////////////////////////////
	constructor( ...args )
	{
		super( ...(parse_args( 4, args )) );
	}
	/////////////////////////////////////////////////////////////////////////////////////
	static get ELEMENTS()
	{
		return 4;
	}
	static get BYTE_LENGTH()
	{
		return 4 * Uint8Array.BYTES_PER_ELEMENT;
	}
	get isRGB()   { return true;           }
	get isColor() { return true;           }
	get r()       { return this[0];        }
	get g()       { return this[1];        }
	get b()       { return this[2];        }
	get a()       { return this[3];        }
	set r(v)      { this[0] = v   ;        }
	set g(v)      { this[1] = v   ;        }
	set b(v)      { this[2] = v   ;        }
	set a(v)      { this[3] = v   ;        }
	get clone()   { return new RGBA(this); }
	get brightness()
	{
		return (this[0] * 299 + this[1] * 587 + this[2] * 114) / 1000;
	}
	get luminance()
	{
		return (0.2126 * RGB(this[0])) + (0.7152 * RGB(this[1])) + (0.0722 * RGB(this[2]));
	}
/*
	get hsla()
	{
		let [r, g, b, a] = [Cs(this[0]),  Cs(this[1]),  Cs(this[2]),  Cs(this[3]) ];
		let [max, min  ] = [Math.max(r, g, b), Math.min(r, g, b)];
		let [h, s, l, d] = [0, 0, ((max + min) / 2), (max - min)];

		if (max !== min)
		{
			s = (l > 0.5) ? (d / (2 - max - min)) : (d / (max + min));
			switch(max)
			{
				case r: h = (g - b) / d + (g < b ? 6 : 0); break;
				case g: h = (b - r) / d + 2;               break;
				case b: h = (r - g) / d + 4;               break;
			}
			h /= 6;
		}
		return new HLSA([h * 360, s, l, a]);
	}
*/
	/////////////////////////////////////////////////////////////////////////////////////
	set( r,g,b,a )
	{
		[this[0], this[1], this[2], this[3]] = [r,g,b,a];
		return this;
	}
	blend( other, fn=BlendMode.average )
	{
		let [ r,  g,  b,  a] = [Cs(this[0]),  Cs(this[1]),  Cs(this[2]),  Cs(this[3])       ];
		let [or, og, ob, oa] = [Cs(other[0]), Cs(other[1]), Cs(other[2]), Cs(other[3])      ];
		let [rr, rb, rg, ra] = [fn(r,or),     fn(r,or),     fn(r,or),     (a + oa * (1 - a))];
		if( ra )
		{
			rr = (oa * or + a * (r - oa * (a + or - ra))) / ra;
			rg = (oa * og + a * (g - oa * (a + og - ra))) / ra;
			rb = (oa * ob + a * (b - oa * (a + ob - ra))) / ra;
		}
		[this[0],this[1],this[2],this[3]] = [rr, rb, rg, ra];
		return this;
	}
	/////////////////////////////////////////////////////////////////////////////////////
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
class HSLA extends Float32Array
{
	/////////////////////////////////////////////////////////////////////////////////////
	constructor( ...args )
	{
		super( ...(parse_args( 4, args )) );
	}
	/////////////////////////////////////////////////////////////////////////////////////
	static get ELEMENTS()
	{
		return 4;
	}
	static get BYTE_LENGTH()
	{
		return 4 * Float32Array.BYTES_PER_ELEMENT;
	}
	get isHLS()   { return true;           }
	get isColor() { return true;           }
	get h()       { return this[0];        }
	get l()       { return this[1];        }
	get s()       { return this[2];        }
	get a()       { return this[4];        }
	get clone()   { return new HLSA(this); }

}
class sRGBA extends Float32Array
{

}
class HSL extends Float32Array
{

}
*/
module.exports =
{
	RGBA,
	BlendMode
}