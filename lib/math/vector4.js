const { vec4            } = require( "./gl-matrix/index" );
const { Vector_Template } = require( "./vector.base"     );

class Vector4 extends Vector_Template( vec4, 4 )
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	static transformQuat( vec4, quat )
	{
		return vec4.transformQuat( new Vector4(), vec4, quat );
	}
	static fromObject( {x=0.0, y=0.0, z=0.0, w=0.0}, out=(new Vector4()) )
	{
		return vec4.set( out, x, y, z, w );
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	get isVector4(){ return true;       }
	get x()        { return this[ 0 ];  }
	get y()        { return this[ 1 ];  }
	get z()        { return this[ 2 ];  }
	get w()        { return this[ 3 ];  }
	set x( value ) { this[ 0 ] = value; }
	set y( value ) { this[ 1 ] = value; }
	set z( value ) { this[ 2 ] = value; }
	set w( value ) { this[ 3 ] = value; }
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	setFromObject( {x=0.0, y=0.0, z=0.0, w=0.0} )
	{
		return vec4.set( this, x, y, z, w );
	}
	transformQuat( quat )
	{
		return vec4.transformQuat( this, this, quat );
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}
module.exports = {Vector4};