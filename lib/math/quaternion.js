const { quat         } = require( "./gl-matrix/index" );
const { Vector3      } = require( "./vector3"         );
const { Vector4      } = require( "./vector4"         );
const { Matrix4      } = require( "./matrix4"         );

class Quaternion extends Vector4
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	static rotation( a, b )
	{
		return quat.rotationTo( new Quaternion(), a, b );
	}
	static setAxes( v3_view, v3_right, v3_up )
	{
		return quat.setAxes( new Quaternion(), v3_view, v3_right, v3_up );
	}
	static get identity()
	{
		return quat.identity( new Quaternion() );
	}
	static setToIdentity()
	{
		return quat.identity( new Quaternion() );
	}
	static setAxisAngle( v3_axis, rad )
	{
		return quat.setAxisAngle( new Quaternion(), v3_axis, rad );
	}
	static getAxisAngle( q, v3_axis = new Vector3() )
	{
		return { axis: v3_axis, radians: quat.getAxisAngle( v3_axis, q ) };
	}
	static multiply( a, b )
	{
		return quat.multiply( new Quaternion(), a, b );
	}
	static mul( a, b )
	{
		return quat.mul( new Quaternion(), a, b );
	}
	static rotateX( q, radians )
	{
		return quat.rotateX( new Quaternion(), q, radians );
	}
	static rotateY( q, radians )
	{
		return quat.rotateY( new Quaternion(), q, radians );
	}
	static rotateZ( q, radians )
	{
		return quat.rotateZ( new Quaternion(), q, radians );
	}
	static calculateW( q )
	{
		return quat.calculateW( new Quaternion(), q );
	}
	static slerp( q_a, q_b, factor )
	{
		return quat.slerp( new Quaternion(), q_a, q_b, factor );
	}
	static sqlerp( q_a, q_b, q_c, q_d, factor )
	{
		return quat.sqlerp( new Quaternion(), q_a, q_b, q_c, q_d, factor );
	}
	static invert( q )
	{
		return quat.invert( new Quaternion(), q );
	}
	static conjugate( q )
	{
		return quat.conjugate( new Quaternion(), q );
	}
	static fromMat3( mat3 )
	{
		return quat.fromMat3( new Quaternion(), mat3 );
	}
	static toMat4( quat )
	{
		return Matrix4.fromQuat( this );
	}
	static str( q )
	{
		return quat.str( q );
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	constructor( ...args )
	{
		super( ...args );
		quat.identity( this );
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	get isQuaternion()
	{
		return true;
	}
	get str()
	{
		return quat.str( this );
	}
	get mat4()
	{
		return Matrix4.fromQuat( this );
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	rotationTo( b )
	{
		return quat.rotationTo( this, this, b );
	}
	setAxes( v3_view, v3_right, v3_up )
	{
		return quat.setAxes( this, v3_view, v3_right, v3_up );
	}
	toIdentity()
	{
		return quat.identity( this );
	}
	setAxisAngle( v3_axis, rad )
	{
		return quat.setAxisAngle( this, v3_axis, rad );
	}
	getAxisAngle( v3_axis = new Vector3() )
	{
		return { axis: v3_axis, radians: quat.getAxisAngle( v3_axis, this ) };
	}
	multiply( other )
	{
		return quat.multiply( this, this, other );
	}
	mul( other )
	{
		return quat.mul( this, this, other );
	}
	rotateX( radians )
	{
		return quat.rotateX( this, this, radians );
	}
	rotateY( radians )
	{
		return quat.rotateY( this, this, radians );
	}
	rotateZ( radians )
	{
		return quat.rotateZ( this, this, radians );
	}
	calculateW()
	{
		return quat.calculateW( this, this );
	}
	slerp( q_a, q_b, factor )
	{
		return quat.slerp( this, q_a, q_b, factor );
	}
	sqlerp( q_a, q_b, q_c, q_d, factor )
	{
		return quat.sqlerp( this, q_a, q_b, q_c, q_d, factor );
	}
	invert()
	{
		return quat.invert( this, this );
	}
	conjugate()
	{
		return quat.conjugate( this, this );
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

module.exports = {Quaternion};