const { mat4            } = require( "./gl-matrix/index" );
const { Matrix_Template } = require( "./matrix.base"     );
const { Vector3         } = require( "./vector3"         );

class Matrix4 extends Matrix_Template( mat4, 16 )
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	static perspective( fovy, aspect, near, far, out=(new Matrix4()) )
	{
		return mat4.perspective( out, fovy, aspect, near, far );
	}
	static perspectiveFromFieldOfView( fov, near, far, out=(new Matrix4()) )
	{
		return mat4.perspectiveFromFieldOfView( out, fov, near, far );
	}
	static ortho( left, right, bottom, top, near, far, out=(new Matrix4()) )
	{
		return mat4.ortho( out, left, right, bottom, top, near, far );
	}
	static frustum( left, right, bottom, top, near, far, out=(new Matrix4()) )
	{
		return mat4.frustum( out, left, right, bottom, top, near, far );
	}
	static lookAt( eye, center, up, out=(new Matrix4()) )
	{
		return mat4.lookAt( out, eye, center, up );
	}
	static fromTranslation( vec3, out=(new Matrix4()) )
	{
		return mat4.fromTranslation( out, vec3 );
	}
	static fromRotation( radians, axis, out=(new Matrix4()) )
	{
		return mat4.fromXRotation( out, radians, axis );
	}
	static fromXRotation( radians, out=(new Matrix4()) )
	{
		return mat4.fromXRotation( out, radians );
	}
	static fromYRotation( radians, out=(new Matrix4()) )
	{
		return mat4.fromYRotation( out, radians );
	}
	static fromZRotation( radians, out=(new Matrix4()) )
	{
		return mat4.fromZRotation( out, radians );
	}
	static fromRotationTranslation( q_rotation, v3_translation, out=(new Matrix4()) )
	{
		return mat4.fromRotationTranslation( out, q_rotation, v3_translation );
	}
	static fromRotationTranslationScale( q_rotation, v3_translation, v3_scaling, out=(new Matrix4()) )
	{
		return mat4.fromRotationTranslationScale( out, q_rotation,  v3_translation, v3_scaling );
	}
	static fromRotationTranslationScaleOrigin( q_rotation, v3_translation, v3_scaling, v3_origin, out=(new Matrix4()) )
	{
		return mat4.fromRotationTranslationScaleOrigin( out, q_rotation, v3_translation, v3_scaling, v3_origin, out=(new Matrix4()) );
	}
	static fromQuat( quat, out=(new Matrix4()) )
	{
		return mat4.fromQuat( out, quat );
	}
	static transpose( mat4, out=(new Matrix4()) )
	{
		return mat4.transpose( out, mat4 );
	}
	static adjoint( mat4, out=(new Matrix4()) )
	{
		return mat4.adjoint( out, mat4 );
	}
	static translate( mat4, vec3, out=(new Matrix4()) )
	{
		return mat4.translate( out, mat4, vec3 );
	}
	static rotate( mat4, radians, axis, out=(new Matrix4()) )
	{
		return mat4.rotate( out, mat4, radians, axis );
	}
	static rotateX( mat4, radians, out=(new Matrix4()) )
	{
		return mat4.rotateX( out, mat4, radians );
	}
	static rotateY( mat4, radians, out=(new Matrix4()) )
	{
		return mat4.rotateY( out, mat4, radians );
	}
	static rotateZ( mat4, radians, out=(new Matrix4()) )
	{
		return mat4.rotateZ( out, mat4, radians );
	}
	static translation( mat4, out=(new Vector3()) )
	{
		return mat4.getTranslation( out, mat4 );
	}
	static scaling( mat4, out=(new Vector3()) )
	{
		return mat4.getScaling( out, mat4 );
	}
	static rotation( mat4, out=(new Quaternion()) )
	{
		return mat4.getRotation( out, mat4 );
	}
	static decomposition( mat4, out_t=(new Vector3()), out_r=(new Quaternion()), out_s=(new Vector3()) )
	{
		return {
			translation : Matrix4.translation( this, out_t ),
			rotation    : Matrix4.rotation( this, out_r ),
			scale       : Matrix4.scaling( this, out_s )
		};
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	constructor( ...args )
	{
		super( ...args );
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	get isMatrix4()
	{
		return true;
	}
	get translation()
	{
		return mat4.getTranslation( new Vector3(), this );
	}
	get scaling()
	{
		return mat4.getScaling( new Vector3(), this );
	}
	get rotation()
	{
		return mat4.getRotation( new Quaternion(), this );
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	compose( r, t, s )
	{
		Matrix4.fromRotationTranslationScale( r, t, s, this );
		return this;
	}
	decompose( r=(new Quaternion()), t=(new Vector3()), s=(new Vector3()) )
	{
		Matrix4.translation( this, t );
		Matrix4.rotation( this, r );
		Matrix4.scaling( this, s );
		return this;
	}
	lookAt( eye, center, up )
	{
		return mat4.lookAt( this, eye, center, up );
	}
	transpose()
	{
		return mat4.transpose( this, this );
	}
	adjoint()
	{
		return mat4.adjoint( this, this );
	}
	translate( vec3 )
	{
		return mat4.translate( this, this, vec3 );
	}
	rotate( radians, axis )
	{
		return mat4.rotate( this, this, radians, axis );
	}
	rotateX( radians )
	{
		return mat4.rotateX( this, this, radians );
	}
	rotateY( radians )
	{
		return mat4.rotateY( this, this, radians );
	}
	rotateZ( radians )
	{
		return mat4.rotateZ( this, this, radians );
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

module.exports = {Matrix4};