
class Clock
{
	constructor( auto=true )
	{
		this.auto    = start;
		this.first    = performance.now();
		this.previous = 0.0;
		this.elapsed  = 0.0;
		this.running  = false;
	}
	start()
	{
		if( !this.running )
		{
			this.first    = performance.now();
			this.previous = this.first;
			this.elapsed  = 0.0;
			this.running  = true;
		}
		return this;
	}
	restart()
	{
		return this.stop().start();
	}
	stop()
	{
		this.running = false;
		return this;
	}
	get delta()
	{
		let next = 0.0, result = 0.0;

		if( this.start && !this.running ) this.start();
		if( this.running )
		{
			next          = performance.now();
			this.elapsed += (result = next - this.previous);
			this.previous = next;
		}
		return result;
	}
}