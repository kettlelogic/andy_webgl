require( '../ext/window' );
const { Vector2, Vector3, Vector4, Quaternion, Matrix2, Matrix2d, Matrix3, Matrix4 } = require( '../math/matrix');
const Uniform = require( '../webgl/uniform' );
const Mouse   = require( '../io/mouse' );
const Object3D = require( './object' );


class Camera extends Object3D
{
	static get isCamera() { return true; }

	constructor()
	{
		super();
		this.view       = new Uniform.Matrix4('View');
		this.projection = new Uniform.Matrix4('Projection')
	}
	get isCamera() { return true; }
	lookAt( vector )
	{
		this.matrix.lookAt( this.translation, vector, this.up );
	}
	updateView()
	{
		this.updateMatrix();
		Matrix4.invert( this.matrix, this.view );
		return this;
	}

	enable()
	{
		this.view.enable();
		this.projection.enable();
		return this;
	}
	disable()
	{
		this.view.disable();
		this.projection.disable();
		return this;
	}

}

class PerspectiveCamera extends Camera
{
	static get isPerspective() { return true; }

	constructor( fov=50, aspect=window.aspectRatio(), near=0.1, far=2000 )
	{
		super();
		this.fov    = fov;
		this.near   = near;
		this.far    = far;
		this.aspect = aspect;

		this.updateProjection();
	}
	get isPerspective() { return true; }

	updateProjection()
	{
		Matrix4.perspective( this.fov, this.aspect, this.near, this.far, this.projection );
		return this;
	}

	update( delta )
	{
		console.log( delta.x,delta.y );
		this.rotateX( -(deg2rad(delta.y/100)) );
		this.rotateY( -(deg2rad(delta.x/100)) );
	}

}

module.exports = PerspectiveCamera;

/*
const c = (v)=>(a,b)=>( Vector3.add(a,b,v).normalize() )

class Camera
{
	constructor( fov=70, near=0.0, far=100.0 )
	{
		this.position   = new Vector3([0.0,0.0, 0.0]);
		this.target     = new Vector3([0.0,0.0,-1.0]);
		this.up         = new Vector3([0.0,1.0, 0.0]);
		this._center    = new Vector3();
		this._view      = new Uniform.Matrix4('View');	// world to view transformation
		this.projection = new Uniform.Matrix4('Projection').copy( Matrix4.perspective( fov, window.aspectRatio(), near, far ) );
		this.dirty      = true;

		Mouse.delta.on( 'change', this.update.bind(this) );
	}
	get center()
	{
		if( this.dirty )
		{
			Vector3.add( this.target, this.position, this._center ).normalize();
			this.dirty = false;
		}
		return this._center;
	}
	get view()
	{
		if( this.dirty )
		{
			this._view.lookAt( this.position, this.center, this.up );
			this.dirty = false;
		}
		return this._view;
	}
	update( delta )
	{
		this.target.rotateX( (delta.y/1000) );
		this.target.rotateY( (delta.x/1000) );
		this.dirty = true;
	}
	enable()
	{
		this.view.enable();
		this.projection.enable();
		return this;
	}
	disable()
	{
		this.view.disable();
		return this;
	}
	move( x,y,z )
	{
		this.position.add( [x/100,y/100,z/100] );
		this.dirty = true;
	}
}
module.exports = Camera;
*/
