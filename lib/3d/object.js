const { Vector2, Vector3, Vector4, Quaternion, Matrix2, Matrix2d, Matrix3, Matrix4 } = require("../math/matrix")

const _axis =
{
	x : Vector3.from([1,0,0]),
	y : Vector3.from([0,1,0]),
	z : Vector3.from([0,0,1]),
};

class Object3D
{
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	static get up()
	{
		return axis.y;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	constructor()
	{
		this.data        = new ArrayBuffer( Vector3.BYTES*3 + Quaternion.BYTES + Matrix4.BYTES );
		this.translation = new Vector3(    this.data, Vector3.BYTES*0                    ).set( 0,0,0 );
		this.scale       = new Vector3(    this.data, Vector3.BYTES*1                    ).set( 1,1,1 );
		this.up          = new Vector3(    this.data, Vector3.BYTES*2                    ).set( ..._axis.y );
		this.rotation    = new Quaternion( this.data, Vector3.BYTES*3                    );
		this.matrix      = new Matrix4(    this.data, (Vector3.BYTES*3)+Quaternion.BYTES );
		this.dirty = false;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	applyMatrix( mat4 )
	{
		this.matrix.multiply( mat4 );
		this.matrix.decompose( this.rotation, this.translation, this.scale );
		this.dirty = true;
		return this;
	}
	updateMatrix()
	{
		this.matrix.compose( this.rotation, this.translation, this.scale );
		this.dirty = true;
		return this;
	}

	rotateX( rad )
	{
		this.rotation.rotateX( rad );
		this.dirty = true;
		return this;
	}
	rotateY( rad )
	{
		this.rotation.rotateY( rad );
		this.dirty = true;
		return this;
	}
	rotateZ( rad )
	{
		this.rotation.rotateZ( rad );
		this.dirty = true;
		return this;
	}
	rotate( x_rad ,y_rad ,z_rad )
	{
		this.rotation.rotateX( x_rad );
		this.rotation.rotateY( y_rad );
		this.rotation.rotateZ( z_rad );
		this.dirty = true;
		return this;
	}

	translate( x_dist, y_dist, z_dist )
	{
		this.translation.scaleAndAdd( _axis.x, x_dist );
		this.translation.scaleAndAdd( _axis.y, y_dist );
		this.translation.scaleAndAdd( _axis.z, z_dist );
		this.dirty = true;
		return this;
	}
	translateX( dist )
	{
		this.translation.scaleAndAdd( _axis.x, dist );
		this.dirty = true;
		return this;
	}
	translateY( dist )
	{
		this.translation.scaleAndAdd( _axis.y, dist );
		this.dirty = true;
		return this;
	}
	translateZ( dist )
	{
		this.translation.scaleAndAdd( _axis.z, dist );
		this.dirty = true;
		return this;
	}
	lookAt( vector )
	{
		this.matrix.lookAt( vector, this.translation, this.up );
		this.dirty = true;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
}

module.exports = Object3D;