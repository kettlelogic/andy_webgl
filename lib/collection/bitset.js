const { nextNearestIncrement, bit } = require( '../math/math' );

const _MAX_  = 4294967295;
const _BITS_ = 32;

const index = ( n )=>( ( n/32 )|0 );
const check = ( value, n )=>( (value & bit(n) ) !== 0 );
const offset = ( n )=>( (n%32)|0);

class Bitset extends Uint32Array
{
	get LENGTH()
	{
		return this.size;
	}
	get BYTES()
	{
		return length * Uint32Array.BYTES_PER_ELEMENT;
	}

	constructor( buffer_or_length, offset=0, length=buffer_or_length )
	{
		super( index(nextNearestIncrement(32,length)) );
		this.size  = length;
		this.count = 0;
	}

	get full()
	{
		return !(this.count < this.size);
	}
	get empty()
	{
		return this.count === 0;
	}
	isset( i, ...rest )
	{
		return !(rest.length) ? ( check(this[index(i)],offset(i)) ) : ( check(this[index(i)],offset(i)) && this.isset(...rest) );
	}

	set( i, ...rest )
	{
		if( !this.isset(i) )
		{
			this.count++;
			this[ index(i) ] |= bit(i);
		}
		return !!(rest.length) ? this.set( ...rest ) : this;
	}
	clear( i, ...rest )
	{
		if( !i && !rest.length )
		{
			return this.fill( false )
		}
		if( this.isset(i) )
		{
			this.count--;
			this.values[ index(i) ] &= ~bit(i);
		}
		return !!(rest.length) ? this.clear( ...rest ) : this;
	}
	fill( b, start=0, end=this.length )
	{
		Uint32Array.prototype.fill.call( this, ((b?1:0)*4294967295)|0, start, end )
		this.used += (b?1:-1)*(end-start);
		return this;
	}

	first( bool=false, i=0 )
	{
		if( i < 0 ) return -1;

		let value  = 0|0;
		let offset = (i%32)|0
		let n      = index(i);

		while( i < this.size )
		{
			value = this[n];
			if( ( ((value === 4294967295) && !bool) ) || ( ((value === 0) && bool) ) )
			{
				n++;
				continue;
			}
			break;
		}
		if( n*32 !== i ) offset = 0;
		while( offset < 32 )
		{
			if( ( !bool && ((value&bit(offset)) !== 0) ) || (  bool && ((value&bit(offset)) === 0) ) )
			{
				offset++;
				continue;
			}
			break;
		}
		return ((i=(n*32 + offset)|0) < this.size) ? (i) : (-1);
	}

	resize( length )
	{

	}
}
module.exports = Bitset;
/*
let n = new Bitset( 100 );
let a = 0;
console.log( n.first(true) );
while( a < 42 )
{
	a = n.first();
	n.set( a );
}
console.log( n.toString() );
*/