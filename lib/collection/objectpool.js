const   List                   = require( '../collection/abstract.list' );
const   Bitset                 = require( '../collection/bitset' );
const { nextNearestIncrement } = require( '../math/math' );
const SubClassFactory = require( '../util/subclass.factory' );

const $pool   = Symbol( 'pool'   );
const $bucket = Symbol( 'bucket' );
const $index  = Symbol( 'index'  );

///////////////////////////////////////////////////////////////////////////////////////////////////
const ObjectPoolHandle = SubClassFactory(
	function Handle( TYPE )
	{
		return class ObjectPoolHandleType extends TYPE
		{
			static get isObjectPoolHandleType()
			{
				return true;
			}
			constructor()
			{
				this[$pool  ] = null;
				this[$bucket] = null;
				this[$index ] = 0;
			}
			get isObjectPoolHandle()
			{
				return true;
			}
		}
	}
);
///////////////////////////////////////////////////////////////////////////////////////////////////
class ObjectPool extends List.List( Object )
{
	static get Handle()
	{
		return ObjectPoolHandle;
	}
	static swap( a, b )
	{
		[a[$bucket][a[$index]], b[$bucket][b[$index]]] = [b,a];
		[a[$pool],a[$bucket],a[$index], b[$pool],b[$bucket],b[$index]] = [b[$pool],b[$bucket],b[$index], a[$pool],a[$bucket],a[$index]];
		return this;
	}

	constructor( factory, bucketSize=1024 )
	{
		this.factory    = handleType;
		this.bucketSize = bucketSize;
		this.usage      = 0;
		this.available  = 0;
		this.reserved   = 0;
	}
	get capacity()
	{
		return this.bucketSize * List.length( this );
	}
	get available()
	{
		return (this.capacity - this.usage)|0;
	}
	reserve( count )
	{
		this.reserved = count;
		if( count > this.capacity )
		{
			this.expand( (count - this.capacity)|0 );
		}
		return this;
	}
	expand( count=this.bucketSize )
	{
		count = (nextNearestIncrement( this.bucketSize, count ))|0;
		while( count > 0 )
		{
			List.push( this, new Bucket( this ) );
			this.available += this.bucketSize;
			count--;
		}
		return this;
	}
	acquire( count=1, accum=[] )
	{
		let bucket = List.head( this );
		let found  = 0;
		let index  = 0;

		while( count > 0 )
		{
			if( bucket.full )
			{
				if( !(bucket = List.next( bucket )) )
				{
					bucket = List.tail( this.expand() );
					index = 0;
				}
			}
			accum.push( bucket[index = bucket.index.first( false, index )] );
			bucket.index.set( index );
			count--;
			this.usage++;
		}
		return (count > 1) ? accum : accum[0];
	}
	release( handle )
	{
		handle[$bucket].index.clear( handle[$index] );
		this.usage--;
		if( handle[$bucket].empty && (this.capacity - this.bucketSize) >= this.reserved )
			List.remove( this, handle[$bucket] );
		return this;
	}
	compact()
	{
		let bucket     = List.head( this ),
		    nextBucket = List.next( bucket );

		while( bucket && nextBucket )
		{
			if( bucket.full )
			{
				nextBucket = List.next( (bucket = nextBucket) );
				continue;
			}
			if( nextBucket.empty )
			{
				nextBucket = List.next( nextBucket );
				continue;
			}
			bucket.compact().concat( nextBucket );
		}
		if( bucket && !nextBucket )
		{
			bucket.compact();
		}
		return this;
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////////
class Bucket extends List.Node( Array )
{
	static bind( handle, pool, bucket, index )
	{
		handle[$pool  ] = pool;
		handle[$bucket] = this;
		handle[$index ] = index;
		return handle;
	}

	constructor( pool, factory )
	{
		super( pool.bucketSize );
		this.index = new Bitset( size );

		for( let i=0; i<this.length; i++ )
		{
			Bucket.bind( (this[i] = pool.factory()), pool, this, i );
		}
	}
	get full()
	{
		return this.index.full;
	}
	get empty()
	{
		return this.index.empty;
	}
	compact()
	{
		let src = 0, dst = 0;

		if( !this.empty )
		{
			while( (src=this.index.first(true, (dst=this.index.first(false, src)) )) !== -1 )
			{
				Pool.swap( this[src], this[dst] );
			}
		}
		return this;
	}
	concat( other )
	{
		let src = 0, dst = 0;
		while( !this.full && !other.empty )
		{
			src = other.index.first( true, src );
			dst = this.index.first( false, src );
			Pool.swap( other[src], this[dst] );
		}
		return this;
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////////

module.exports = ObjectPool;