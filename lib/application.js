const Electron    = require( 'electron' );
const Event       = require( './event' );
const { Vector2 } = require( './math/vector2' );

let   _size      = new Vector2();
let   _position  = new Vector2();
let   _init_done = false;
let   _events    = new Event.Emitter();

class Application
{
	static get position() 	{ return _position; 	}
	static get x() 			{ return _position[0]; 	}
	static get y() 			{ return _position[1]; 	}

	static get size() 		{ return _size; 		}
	static get width() 		{ return _size[0]; 		}
	static get height() 	{ return _size[1]; 		}

	static init()
	{
		if( _init_done ) return;

		window.addEventListener( 'unresponsive', 			Application.onUnresponsive 			);
		window.addEventListener( 'responsive', 				Application.onResponsive 			);
		window.addEventListener( 'blur', 					Application.onBlur 					);
		window.addEventListener( 'focus', 					Application.onFocus 				);
		window.addEventListener( 'show', 					Application.onShow 					);
		window.addEventListener( 'hide', 					Application.onHide 					);
		window.addEventListener( 'ready-to-show', 			Application.onReady 				);
		window.addEventListener( 'maximize', 				Application.onMaximize 				);
		window.addEventListener( 'unmaximize', 				Application.onUnmaximize 			);
		window.addEventListener( 'minimize', 				Application.onMinimize 				);
		window.addEventListener( 'restore', 				Application.onRestore 				);
		window.addEventListener( 'resize', 					Application.onResize 				);
		window.addEventListener( 'move', 					Application.onMove 					);
		window.addEventListener( 'enter-full-screen', 		Application.onFullscreenEnter 		);
		window.addEventListener( 'leave-full-screen', 		Application.onFullscreenExit 		);
		window.addEventListener( 'enter-html-full-screen', 	Application.onFullscreenHtmlEnter 	);
		window.addEventListener( 'leave-html-full-screen', 	Application.onFullscreenHtmlExit 	);
		window.addEventListener( 'load',                    Application.onLoad                  );

		_init_done = true;
		Application.updateBounds();
	}
	// UTILITY FUNCTIONS //////////////////////////////////////////////////////////////////////////
	static updateBounds()
	{
		let bounds = Electron.remote.getCurrentWindow().getContentBounds();
		_position.set( bounds.x, bounds.y );
		_size.set( bounds.width, bounds.height );
	}
	// EVENT EMITTER //////////////////////////////////////////////////////////////////////////////
	static on( event, fn )
	{
		_events.on( event, fn );
		return Application;
	}
	static clear( event, fn )
	{
		_events.clear( event, fn );
		return Application;
	}
	static has( event, fn )
	{
		_events.has( event, fn );
		return Application;
	}
	static emit( event, ...args )
	{
		_events.emit( event, ...args );
		return Application;
	}
	// WINDOW EVENT CALLBACKS /////////////////////////////////////////////////////////////////////
	static onBlur()
	{

		_events.emit('blur');
	}
	static onClosed()
	{

		_events.emit('closed');
	}
	static onFocus()
	{

		_events.emit('focus');
	}
	static onFullscreenEnter()
	{
		Application.updateBounds();
		_events.emit('fullscreen');
	}
	static onFullscreenExit()
	{
		Application.updateBounds();
		_events.emit('windowed');
	}
	static onFullscreenHtmlEnter()
	{
		Application.updateBounds();
		_events.emit('htmlfullscreen');
	}
	static onFullscreenHtmlExit()
	{
		Application.updateBounds();
		_events.emit('htmlwindowed');
	}
	static onHide()
	{

		_events.emit('hide');
	}
	static onMaximize()
	{
		Application.updateBounds();
		_events.emit('maximize');
	}
	static onMinimize()
	{

		_events.emit('minimize');
	}
	static onMove()
	{
		Application.updateBounds();
		_events.emit('move');
	}
	static onReady()
	{
		Application.updateBounds();
		_events.emit('ready');
	}
	static onResize()
	{
		Application.updateBounds();
		_events.emit('resize');
	}
	static onResponsive()
	{

		_events.emit('responsive');
	}
	static onRestore()
	{
		Application.updateBounds();
		_events.emit('restore');
	}
	static onShow()
	{
		Application.updateBounds();
		_events.emit('show');
	}
	static onUnmaximize()
	{
		Application.updateBounds();
		_events.emit('unmaximize');
	}
	static onUnresponsive()
	{

		_events.emit('unresponsive');
	}
	static onLoad()
	{
		_events.emit('load');
	}
	///////////////////////////////////////////////////////////////////////////////////////////////

}

Application.init();

module.exports = Application;