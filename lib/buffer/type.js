const { isArrayBufferViewType } = require('../util');

const $layout = Symbol('layout');
const $type   = Symbol('type');

class HandleArray extends Array
{
	constructor( type, length, buffer=(new ArrayBuffer(type.size*(length|0))), offset=0 )
	{
		super( length );
		this.buffer = buffer;
		this.data   = new DataView( this.buffer );
		this.type   = type;
		this.offset = offset;

		let size = type.prototype[$type].size;

		for( let i=0; i<length; i++ ) this[i] = new (type)( buffer, (offset+i*size)|0 );
	}
	each( f )
	{
		this.forEach( f );
	}
}
/****************************************************************************************
	Type base class
****************************************************************************************/

const _ViewTypes_ = new Map();

class Type
{
	static get( o, x )
	{
		if( o.isPrimitive ) return o;
		if( o.isView      ) return o;
		if( o.isHandle    ) return o.prototype[$type];

		if( !!(x = _ViewTypes_.get(o)) ) return x;
	}
	static is(x)
	{
		return Type.prototype.isPrototypeOf(x);
	}
	constructor( size=0, length=0, uniform=true )
	{
		this.size    = size;
		this.length  = length;
		this.uniform = uniform;
	}
	get isType() { return true; }
}
/****************************************************************************************
	Primitive corresponds to a single native value of a specific type : int, int8,
	... uint, ... float, ..., etc.
****************************************************************************************/
class Primitive extends Type
{
	static is( o ) { return !!(o.isPrimitive); }
	constructor( array_type, getter, setter )
	{
		super( array_type.BYTES_PER_ELEMENT, 1, true );
		this.array  = array_type;
		this.setter = setter;
		this.getter = getter;
	}
	get isPrimitive() { return true; }
	get( data, offset )
	{
		return this.getter.call( data, offset, true );
	}
	set( data, offset, value )
	{
		this.setter.call( data, offset, value, true );
	}
}
/****************************************************************************************
	ViewType corresponds to multiple native values of a single native type stored
	continuously: an array.

	param 'type' should be class extending a TypedArray type which privide the
	BYTE_LENGTH and ELEMENTS properties
****************************************************************************************/
class ViewType extends Type
{
	static is( x )
	{
		return isArrayBufferViewType(x);
	}
	constructor( type )
	{
		super( type.BYTE_LENGTH, type.ELEMENTS, true );
		this.type = type;
		_ViewTypes_.set( type, this );
	}
	get( data, offset )
	{
		return new (this.type)( data, offset, this.length );
	}
	get isView() { return true; }
}
/****************************************************************************************
	Composite corresponds to multiple native values of multiple native type stored
	continuously: a struct.
****************************************************************************************/
function addDescriptor( handle, field )
{
	if( !!field.isPrimitive )
	{
		Object.defineProperty( handle.prototype, field.key,
		{
				enumerable : true,
				get : function()
				{
					return field.type.get( this, field.offset );
				},
				set : function( v )
				{
					field.type.set( this, field.offset, v );
				}
			}
		);
	}
	if( !!field.isView )
	{
		Object.defineProperty( handle.prototype, field.key,
			{
				enumerable   : true,
				configurable : false,
				writable     : true,
				value        : null
			}
		);
	}
	if( !!field.isComposite )
	{
		Object.defineProperty( handle.prototype, field.key,
			{
				enumerable   : true,
				configurable : false,
				writable     : true,
				value        : null
			}
		);
	}
}
class Composite extends Type
{
	constructor( members, ret=true )
	{
		super();
		this.layout = {};
		this.handle = Handle.extend( this );
		this.keys   = Object.keys(members);
		this.uniType= null;
		let type, field, prev=Type.get(members[this.keys[0]]);

		for( let key of this.keys )
		{
			type             = Type.get(members[key]);
			field            = new Field( key, type, this.size );
			this.layout[key] = field;
			this.size        = (this.size   + type.size  )|0;
			this.length      = (this.length + type.length)|0;
			this.uniform     = this.uniform ? (prev === type) : false;

			addDescriptor( this.handle, field );
			prev = type;
		}
		if( this.uniform ) this.uniType = prev;
		if( ret ) return this.handle;
	}
	get isComposite() { return true; }
}
/****************************************************************************************
	Field store Composite instance layout information
****************************************************************************************/
class Field
{
	constructor( key, type, offset  )
	{
		this.key    = key;
		this.type   = type;
		this.offset = offset;
	}
	get isField() { return true;            }
	get size()    { return this.type.size;  }
	get length()  { return this.type.length }

	get isPrimitive() { return !!this.type.isPrimitive; }
	get isComposite() { return !!this.type.isComposite; }
	get isView()      { return !!this.type.isView;      }
	get isUniform()   { return !!this.type.uniform;     }
}
/****************************************************************************************
	Handle is a base class for instances of a Composite
****************************************************************************************/
class Handle extends DataView
{
	static is()
	{
		return Handle.prototype.isPrototypeOf(x);
	}
	constructor( data, offset, length )
	{
		super( data, offset, length );
	}
	static extend( type )
	{
		let handle_t = class extends Handle
		{
			static is( subject )
			{
				return (subject instanceof handle_t) || handle_t.prototype.isPrototypeOf(subject);
			}
			constructor( data=(new ArrayBuffer(type.size)), offset=0 )
			{
				super( data, offset, type.size );
				Handle.init( type, this );
			}
		};
		Object.defineProperty( handle_t.prototype, $type, {value: type} );

		return handle_t;
	}
	static Array( buffer_or_length, offset=0, length, buffer )
	{
		if( buffer_or_length instanceof ArrayBuffer )
		{
			buffer = buffer_or_length;
			length = length || ((buffer.length - offset) / this.prototype[$type].size)|0;
		}
		else
		{
			length = buffer_or_length|0;
			buffer = new ArrayBuffer( length* this.prototype[$type].size );
		}
		return new HandleArray( this, length, buffer, offset );
	}

	static get typeinfo()
	{
		return this.prototype[$type];
	}
	static get layout()
	{
		return this.prototype[$type].layout;
	}
	static get size()
	{
		return this.prototype[$type].size;
	}
	static get length()
	{
		return this.prototype[$type].length;
	}
	static get uniform()
	{
		return this.prototype[$type].uniform;
	}
	static get isHandle()
	{
		return true;
	}
	static get ELEMENTS()
	{
		return this.prototype[$type].length;
	}
	static get BYTE_LENGTH()
	{
		return this.prototype[$type].size;
	}

	toString()
	{
		let type   = this[$type];
		let v;
		let r = '{ ';
		for( let key of type.keys )
		{
			v = ((this[key] * 100)|0) / 100;
			if( type.isPrimitive ) { r+= `${key}: ${v}, `; continue; }
			r+= `${key}: ${this[key].toString()}, `; continue;
		}
		r+= ' }';
		return r;
	}

	static init( info, instance )
	{
		let buffer = instance.buffer;
		let offset = instance.byteOffset;
		let layout = info.layout;
		let field;

		for( let key of info.keys )
		{
			field = layout[key];
			if( field.isView )
			{
				instance[key] = field.type.get( buffer, (offset + field.offset)|0 );
				continue;
			}
			if( field.isComposite )
			{
				instance[key] = new (field.type.handle)( buffer, (offset + field.offset)|0, field.size );
				continue;
			}
		}
	}
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
module.exports =
{
	Composite : Composite,
	Handle    : Handle,
	Primitive : Primitive,
	Type      : Type,
	View      : ViewType
};