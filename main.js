const electron      = require('electron');
const app           = electron.app;             // Module to control application life.
const BrowserWindow = electron.BrowserWindow;   // Module to create native browser window.

let Window;

app.commandLine.appendSwitch( 'enable-unsafe-es3-apis' );
app.commandLine.appendSwitch( 'use-gl', 'desktop' );
app.commandLine.appendSwitch( 'js-flags','--harmony-simd --harmony-sharedarraybuffer --enable-blink-feature=SharedArrayBuffer --use_strict' );


// Quit when all windows are closed.
app.on('window-all-closed', function()
{
    if (process.platform !== 'darwin')
    {
        app.quit();
    }
});
console.log( process.argv );

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
app.on('ready', function()
{
    Window = new BrowserWindow({width: 1600, height: 800, frame: true });

    Window.loadURL(`file://${__dirname}/.${process.argv[2] || 'app'}/index.html`);
    Window.webContents.openDevTools();
    Window.on('closed', function()
    {
        Window = null;
    });
});