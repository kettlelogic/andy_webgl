
const Keyboard = require('../lib/io/keyboard');

Keyboard.bind( 'W', ()=>(console.log('W down')) );
Keyboard.bind( 'A', ()=>(console.log('A down')) );
Keyboard.bind( 'S', ()=>(console.log('S down')) );
Keyboard.bind( 'D', ()=>(console.log('D down')) );